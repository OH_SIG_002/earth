// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from single_language_interfaces.djinni

package com.dropbox.djinni.test;

import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;

/**
 * Generating and compiling this makes sure other languages don't break
 * on references to interfaces they don't need.
 */
public abstract class UsesSingleLanguageListeners {
    public abstract void callForObjC(@CheckForNull ObjcOnlyListener l);

    @CheckForNull
    public abstract ObjcOnlyListener returnForObjC();

    public abstract void callForJava(@CheckForNull JavaOnlyListener l);

    @CheckForNull
    public abstract JavaOnlyListener returnForJava();
    /** Release the underlying native object */
    public abstract void destroy();


    private static final class CppProxy extends UsesSingleLanguageListeners
    {
        private final long nativeRef;
        private final AtomicBoolean destroyed = new AtomicBoolean(false);

        private CppProxy(long nativeRef)
        {
            if (nativeRef == 0) throw new RuntimeException("nativeRef is zero");
            this.nativeRef = nativeRef;
        }

        private native void nativeDestroy(long nativeRef);
        @Override
        public void destroy()
        {
            boolean destroyed = this.destroyed.getAndSet(true);
            if (!destroyed) nativeDestroy(this.nativeRef);
        }
        protected void finalize() throws java.lang.Throwable
        {
            destroy();
            super.finalize();
        }

        @Override
        public void callForObjC(ObjcOnlyListener l)
        {
            if (this.destroyed.get())
            {
                throw new RuntimeException("trying to use a destroyed object (UsesSingleLanguageListeners)");
            }
            native_callForObjC(this.nativeRef, l);
        }
        private native void native_callForObjC(long _nativeRef, ObjcOnlyListener l);

        @Override
        public ObjcOnlyListener returnForObjC()
        {
            if (this.destroyed.get())
            {
                throw new RuntimeException("trying to use a destroyed object (UsesSingleLanguageListeners)");
            }
            return native_returnForObjC(this.nativeRef);
        }
        private native ObjcOnlyListener native_returnForObjC(long _nativeRef);

        @Override
        public void callForJava(JavaOnlyListener l)
        {
            if (this.destroyed.get())
            {
                throw new RuntimeException("trying to use a destroyed object (UsesSingleLanguageListeners)");
            }
            native_callForJava(this.nativeRef, l);
        }
        private native void native_callForJava(long _nativeRef, JavaOnlyListener l);

        @Override
        public JavaOnlyListener returnForJava()
        {
            if (this.destroyed.get())
            {
                throw new RuntimeException("trying to use a destroyed object (UsesSingleLanguageListeners)");
            }
            return native_returnForJava(this.nativeRef);
        }
        private native JavaOnlyListener native_returnForJava(long _nativeRef);
    }
}
