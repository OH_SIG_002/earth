// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from enum_flags.djinni

#include "empty_flags.hpp"  // my header
#include "enum_from_string.hpp"

namespace testsuite {

std::string to_string(const empty_flags& empty_flags) {
    switch (empty_flags) {
        case empty_flags::NONE: return "NONE";
        case empty_flags::ALL: return "ALL";
        default: return "UNKNOWN";
    };
};
template <>
empty_flags from_string(const std::string& empty_flags) {
    if (empty_flags == "NONE") return empty_flags::NONE;
    else return empty_flags::ALL;
};

std::ostream &operator<<(std::ostream &os, const empty_flags &o)
{
    switch (o) {
        case empty_flags::NONE:  return os << "NONE";
        case empty_flags::ALL:  return os << "ALL";
    }
}

}  // namespace testsuite
