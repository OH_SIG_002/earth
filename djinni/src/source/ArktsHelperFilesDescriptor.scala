package djinni

// -RRR | 2023-06-14 | Changes START | To generate .cpp and .hpp files for the ArkTS folder

import djinni.generatorTools.Spec

class ArktsHelperFilesDescriptor(spec: Spec) {
  val ObjectWrapperName = spec.nodeIdentStyle.ty(ArktsHelperFilesDescriptor.objectWrapperLogicalName)
  val ObjectWrapperHeader = ObjectWrapperName + "." + spec.cppHeaderExt

  val HexUtilsName = spec.nodeIdentStyle.ty(ArktsHelperFilesDescriptor.hexUtilsLogicalName)
  val HexUtilsHeader = HexUtilsName + "." + spec.cppHeaderExt
  val HexUtilsCpp = HexUtilsName + "." + spec.cppExt
}

object ArktsHelperFilesDescriptor {
  protected val objectWrapperLogicalName = "ObjectWrapper"
  protected val hexUtilsLogicalName = "HexUtils"
}

// -RRR | 2023-06-14 | Changes END | To generate .cpp and .hpp files for the ArkTS folder