CPP_OUTPUT_FOLDER=target/sayhello/cpp
ARKTS_OUTPUT_FOLDER=target/sayhello/arkts
MY_PROJECT=target/sayhello/say_hello.djinni


src/run  --arkts-out $ARKTS_OUTPUT_FOLDER --arkts-type-prefix Node --arkts-module SayHello --arkts-include-cpp $CPP_OUTPUT_FOLDER --cpp-optional-header absl/types/optional.h --cpp-optional-template absl:optional --cpp-out $CPP_OUTPUT_FOLDER --idl $MY_PROJECT
