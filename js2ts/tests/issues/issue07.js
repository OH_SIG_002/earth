// global case - output needs "var obj", *unless* "obj" is already declared 
obj = {a:1, b:2};        // needs "var"
console.log (obj);

obj = {a:3, b:4};        // must not have "var"
console.log (obj);

if (obj.a == 3) {
    eobj = {a:5, b:6};	 // needs "var" before if statement
} else {
    eobj = {a:7, b:8};
}
console.log (eobj);

// class property case - output needs to *not* have "var cobj" 
class Main {
    cobj = {b:7,c:8};    // must not have "var"
    constructor () {
        console.log (this.cobj);
    }
}
const mainobj = new Main();

// function case - output needs to have "let fobj", *unless* "fobj" is already declared
function f () {
    fobj = {a:9,b:10};   // needs "let"
    obj = {a:11,b:12};   // must not have "let"
    return (fobj);
}
console.log (f ());
console.log (obj);

