"use strict";
class Hewans {
    constructor(nama, kaki, mamalia) {
        this.nama = nama;
        this.kaki = kaki;
        this.mamalia = mamalia;
    }
    berjalan() {
    }
}
class Kucing extends Hewans {
    constructor() {
        super(...arguments);
        this.mata = "Cokelat";
        this.bulu = "Abu abu";
        this.ekor = true;
    }
    getBiodata() {
        console.log(this.mata + this.bulu + this.ekor);
    }
}
const kucing = new Kucing("Cika", 2, true);
kucing.getBiodata();
console.log(kucing);
