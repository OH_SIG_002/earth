class a {
    foo(...args) {
        if (args[2] == 1) {
            return this._foo(args[0], args[1])
        } else {
            return this._foo(args[0], args[1], args[2])
        }
    }
    _foo(a, b, c) {
        if (c) {
            return a + b + c
        }
        return a + b
    }
}
var b = new a
console.log(b.foo(1, 2, 3))
