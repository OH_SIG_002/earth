// extends null
class Foo extends null {}

// extends function
var V = class extends function() {
        return {};
    } {};
console.log (new V());

// extends expression with side effects
var calls = 0;
class C {}
class D extends (calls++, C) {}
var d = new D();
console.log (calls);
