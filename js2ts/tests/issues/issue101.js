let set_string = new Set(['red', 'green', 'blue'])
let set_number = new Set([1, 2, 3]);
let set_object = new Set([{a:1,b:2},{s1:'1',s2:'2'}])
let set_mix    = new Set([undefined,'hello',100,null,{a:1}])
