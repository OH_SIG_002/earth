function foo (a) {
    if (a === 1) {
        return;
    }
    if (a === 2) {
        return new Error('error')
    }
    if (a === 3) {
        return [String(a), String(a)]
    }
}
var b = foo(1)
var c = foo(3)
console.log(b, c)
