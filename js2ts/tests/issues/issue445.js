const convertObject = (param) => {
    let list = [];
    if (Object.prototype.toString.call(param) === '[object Object]') {
      let keys = Object.keys(param);
      keys.forEach((key) => {
        list.push({
          name: key,
        });
      });
      return list;
    } else {
      switch (Object.prototype.toString.call(param)) {
        case 'regexp':
          return param.toString();
        case 'function':
          return ' ƒ() {...}';
        default:
          return `"${param.toString()}"`;
      }
    }
  };
const obj = { a: 1, b: 2, c: 3 };
const result = convertObject(obj);
console.log(result);
