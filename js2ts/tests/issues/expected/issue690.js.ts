"use strict";
class Mother {
    name : string;
    foot : number;
    constructor (name : string, foot : number) {
        this.name = name;
        this.foot = foot;
    }
}
class Child extends Mother {
    eye : string;
    constructor (name : string, foot : number, eye : string) {
        super (name, foot);
        this.eye = eye;
    }
}
let child : Child = new Child ("Panji Asmoro", 2, "Hitam");
console.log (child);
