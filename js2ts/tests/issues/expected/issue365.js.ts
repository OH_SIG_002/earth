function * counter () : Generator <number, any, any> {
    let count : number = 0;
    while (true) {
        yield count ++;
    }
}
const gen : Generator <number, any, any> = counter ();
var _next : number = gen.next ().value;
console.log (_next);
_next = gen.next ().value;
console.log (_next);
console.log (gen.next ().value);
var _return : number = gen.return (3).value;
console.log (_return);
