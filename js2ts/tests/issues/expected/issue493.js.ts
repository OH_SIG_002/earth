var x : number = 0;
assert.sameValue (x, 0, 'The value of `x` is 0');
var y ! : any;
assert.sameValue (y, undefined, 'The value of `y` is expected to equal `undefined`');
this.y ++;
assert.sameValue (isNaN (y), true, 'isNaN(y) returns true');
