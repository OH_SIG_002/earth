var await ! : number;
async function foo () : Promise <any> {
    function bar () : void {
        await = 1;
    }
    bar ();
}
foo ();
async function callAsync () : Promise <any> {
    await console.log (1);
    await console.log (2);
}
callAsync ();
