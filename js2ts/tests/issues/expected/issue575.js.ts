type JSDynamicObject = any;
var MAX_DP : number = 1E6;
var NAME : string = '[big.js] ';
var INVALID : string = NAME + 'Invalid ';
var INVALID_DP : string = INVALID + 'decimal places';
var INVALID_RM : string = INVALID + 'rounding mode';
var DIV_BY_ZERO : string = NAME + 'Division by zero';
var P : JSDynamicObject = {};
var UNDEFINED : any = void 0;
var NUMERIC : RegExp = /^-?(\d+(\.\d*)?|\.\d+)(e[+-]?\d+)?$/i;
function round (x : any, sd : number, rm : number, more ? : any) : any {
    var xc : number [] = x.c;
    if (rm === UNDEFINED) rm = x.constructor.RM;
    if (sd < 1) {
        more = rm === 3 && (more || ! ! xc [0]) || sd === 0 && (rm === 1 && xc [0] >= 5 || rm === 2 && (xc [0] > 5 || xc [0] === 5 && (more || xc [1] !== UNDEFINED)));
        xc.length = 1;
        if (more) {
            x.e = x.e - sd + 1;
            xc [0] = 1;
        } else {
            xc [0] = x.e = 0;
        }
    }
    return x;
}
function Big () : void {
}
P = Big.prototype;
P.prec = function (this : JSDynamicObject, sd : any, rm : any) : any {
    if (sd !== ~ ~ sd || sd < 1 || sd > MAX_DP) {
        throw Error (INVALID + 'precision');
    }
    return round (new this.constructor (this), sd, rm);
}
;
P.toExponential = function (this : JSDynamicObject, dp : any, rm : any) : any {
    var x : any = this;
    if (dp !== UNDEFINED) {
        if (dp !== ~ ~ dp || dp < 0 || dp > MAX_DP) {
            throw Error (INVALID_DP);
        }
        x = round (new x.constructor (x), ++ dp, rm);
        return x;
    }
}
;
P.toFixed = function (this : JSDynamicObject, dp : any, rm : any) : any {
    var x : any = this;
    if (dp !== UNDEFINED) {
        if (dp !== ~ ~ dp || dp < 0 || dp > MAX_DP) {
            throw Error (INVALID_DP);
        }
        x = round (new x.constructor (x), dp + x.e + 1, rm);
        return x;
    }
}
;
var x : any = new Big ();
console.log (x.prec (1, 2));
console.log (x.toFixed (1, 2));
console.log (x.toExponential (1, 2));
