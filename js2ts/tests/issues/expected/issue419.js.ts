function * gen () : Generator <number, any, any> {
    while (true) {
        try {
            yield 42;
        }
        catch (e : any) {
            console.log ("Error caught!");
        }
    }
}
const g : Generator <number, any, any> = gen ();
g.next ();
var y : IteratorResult <number, any> = g.throw (new Error ("Something went wrong"));
