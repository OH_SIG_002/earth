console.log (Math.E);
function getNatLog10 () : number {
    return Math.LN10;
}
console.log (getNatLog10 ());
function getNatLog2 () : number {
    return Math.LN2;
}
console.log (getNatLog2 ());
function getLog10e () : number {
    return Math.LOG10E;
}
console.log (getLog10e ());
function getLog2e () : number {
    return Math.LOG2E;
}
console.log (getLog2e ());
function calculateCircumference (radius : number) : number {
    return 2 * Math.PI * radius;
}
console.log (calculateCircumference (1));
console.log (Math.PI);
function getRoot1Over2 () : number {
    return Math.SQRT1_2;
}
console.log (getRoot1Over2 ());
function getRoot2 () : number {
    return Math.SQRT2;
}
console.log (getRoot2 ());
