function optionalTransform (key : any, value : any, settings : any, level : number = 0) : string {
    let val : string = value;
    if (! value) {
        val = `{{delete:${level}}}`;
    }
    return val;
}
