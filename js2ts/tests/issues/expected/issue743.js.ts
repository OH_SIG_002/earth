assert.throws (TypeError, function () : void {
    ({
        [Symbol.toPrimitive] : function () : symbol {
            return Symbol ('1');
        }
    }) ** 0n;
}, '({[Symbol.toPrimitive]: function() {return Symbol("1");}}) ** 0n throws TypeError');
assert.throws (TypeError, function () : void {
    0n ** BigInt ({
        [Symbol.toPrimitive] : function () : symbol {
            return Symbol ('1');
        }
    });
}, '0n ** {[Symbol.toPrimitive]: function() {return Symbol("1");}} throws TypeError');
