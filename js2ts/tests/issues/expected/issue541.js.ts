function baseToString (value : string | number) : string {
    return "42";
}
function baseToNumber (value : number | string) : number {
    return 42;
}
function createMathOperation (operator : any, defaultValue : number) : any {
    return (value : string | number, other : string | number) : number | string => {
            if (value === undefined && other === undefined) {
                return defaultValue;
            }
            if (value !== undefined && other === undefined) {
                return value;
            }
            if (other !== undefined && value === undefined) {
                return other;
            }
            if (typeof value === 'string' || typeof other === 'string') {
                value = baseToString (value);
                other = baseToString (other);
            } else {
                value = baseToNumber (value);
                other = baseToNumber (other);
            }
            return operator (value, other);
        }
}
const multiply : any = createMathOperation ((multiplier : any, multiplicand : any) : any => {
        return multiplier * multiplicand;
    }, 1);
console.log (multiply (42, 42));
