type JSDynamicObject = any;
const expected : string [] = ["second", "fractionalSecondDigits", "localeMatcher", "second", "fractionalSecondDigits", "timeZoneName", "formatMatcher",];
const actual : any [] = [];
const options : JSDynamicObject = {
    get second () : string {
        actual.push ("second");
        return "numeric";
    },
    get fractionalSecondDigits () : any {
        actual.push ("fractionalSecondDigits");
        return undefined;
    },
    get localeMatcher () : any {
        actual.push ("localeMatcher");
        return undefined;
    },
    get timeZoneName () : any {
        actual.push ("timeZoneName");
        return undefined;
    },
    get formatMatcher () : any {
        actual.push ("formatMatcher");
        return undefined;
    },
};
new Intl.DateTimeFormat ("en", options);
assert.compareArray (actual, expected);
