type JSDynamicObject = any;
var x : JSDynamicObject = {
    a : 1,
    b : 2,
    c : 3
};
var x1 : string [] = Object.keys (x);
console.log (x1);
var x2 : any [] = Object.entries (x);
console.log (x2);
var x3 : string [] = Object.getOwnPropertyNames (x);
console.log (x3);
var x4 : any [] = Object.getOwnPropertySymbols (x);
console.log (x4);
var x5 : boolean = Object.hasOwn (x, "a");
console.log (x5);
var x6 : boolean = Object.is (x, x);
console.log (x6);
var x7 : boolean = Object.isExtensible (x);
console.log (x7);
var x8 : boolean = Object.isFrozen (x);
console.log (x8);
var x9 : boolean = Object.isSealed (x);
console.log (x9);
var x10 : string [] = Object.keys (x);
console.log (x10);
var x11 : any [] = Object.values (x);
console.log (x11);
