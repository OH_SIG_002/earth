var x : number = 42;
var y : string = "abc";
function f1 (x : number, y : string) : void {
    if (x == 1) {
        let y : number = 42;
        x = 42;
        console.log (y);
    }
    console.log (x);
    console.log (y);
}
function f2 (x : string, y : number) : void {
    x = "abc";
    y = y + 1;
    if (y == 42) {
        let x : boolean = false;
        x = ! x;
        console.log (x);
    }
    console.log (x);
    console.log (y);
}
f1 (1, "abc");
f2 ("abc", 41);
class c1 {
    f1 : any = function (x : boolean, y : string) : void {
        x = ! x;
        y = y + "c";
        console.log (y);
    }
    f2 (x : number, y : boolean) : void {
        x = 42;
        y = y || false;
        console.log (y);
    }
}
var o1 : c1 = new c1 ();
o1.f1 (true, "ab");
console.log (x);
o1.f2 (1, true);
console.log (y);
