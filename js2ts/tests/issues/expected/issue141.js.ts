var f ! : () => number;
var callCount1 : number = 0;
f = function () : number {
    callCount1 = callCount1 + 1;
    return callCount1;
}
;
f ();
console.log (callCount1);
var callCount2 : number = 0;
var g : (x1 ? : number, x2 ? : number, x3 ? : number) => number = function (x : number = 4, y : number = 5, z : number = 6) : number {
    callCount2 ++;
    return x + y + z;
}
;
console.log (g ());
console.log (callCount2);
var probe ! : () => string;
(function () : void {
    var x : string = "inside";
    probe = function () : string {
        return x;
    }
    ;
}
());
var x : string = "outside";
console.log (probe ());
console.log (x);
