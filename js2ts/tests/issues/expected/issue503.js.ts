type JSNoValue = any;
function foo (a : number) : number | JSNoValue {
    if (a === 1) return;
    if (a === 2) return a;
}
console.log (foo (2));
