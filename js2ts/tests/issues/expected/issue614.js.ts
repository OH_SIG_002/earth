type JSDynamicObject = any;
function htmlRemove (this : JSDynamicObject) : void {
    this.innerHTML = "";
}
function htmlConstant (this : JSDynamicObject, value : any) : any {
    return function (this : JSDynamicObject) : void {
        this.innerHTML = value;
    }
    ;
}
function htmlFunction (this : JSDynamicObject, value : any) : any {
    return function (this : JSDynamicObject, ... JSarguments : any []) : void {
        var v : any = value.apply (this, JSarguments);
        this.innerHTML = v == null ? "" : v;
    }
    ;
}
export default function (this : JSDynamicObject, ... JSarguments : any []) : any {
    let value : any = JSarguments [0];
    return JSarguments.length ? this.each (value == null ? htmlRemove : (typeof value === "function" ? htmlFunction : htmlConstant) (value)) : this.node ().innerHTML;
}
