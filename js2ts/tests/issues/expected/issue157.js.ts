var stringSet ! : any;
class C {
    get "doubleQuote" () : string {
        return 'get string';
    }
    set "doubleQuote" (param : any) {
        stringSet = param;
    }
}
;
C.prototype ["doubleQuote"] = 'set string';
console.log (stringSet);
var callCount : number = 0;
class D {
    method (fromLiteral : number = 23, fromExpr : number = 45, fromHole : number = 99) : void {
        callCount = callCount + 1;
    }
}
;
D.prototype.method (undefined, void 0);
console.log (callCount);
