type JSDynamicObject = any;
class Foo {
    i ! : string;
    static s : string;
    c : string;
    constructor (c : string) {
        this.c = c;
    }
}
console.log (new Foo ("constructor property").c);
Foo.s = "added static property";
console.log (Foo.s);
Foo.prototype.i = "added instance property";
var i : Foo = new Foo ("constructor property");
console.log (i.i);
var o : JSDynamicObject = new Foo ("constructor property");
o.o = "added oject property";
console.log (o.o);
