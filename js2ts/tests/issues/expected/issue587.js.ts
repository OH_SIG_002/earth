type JSDynamicObject = any;
const d : JSDynamicObject = {
    Ls : {
        en : {
            name : 'English'
        },
        fr : {
            name : 'French'
        },
        es : {
            name : 'Spanish'
        }
    }
};
function myFunction (args : any []) : void {
    const isStrictWithLocale : boolean = args [3] === true;
    let pl : any = args [2];
    if (! isStrictWithLocale) [,,, pl] = args;
    console.log (d.Ls [pl]);
}
myFunction ([,,, 'fr']);
