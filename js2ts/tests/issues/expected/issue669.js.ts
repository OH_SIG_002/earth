type JSDynamicObject = any;
"use strict";
var NamespaceExample ! : JSDynamicObject;
(function (this : JSDynamicObject, NamespaceExample : any) : void {
    class Hewan {
        name : string;
        constructor () {
            this.name = "Kucing Putih";
        }
    }
    NamespaceExample.Hewan = Hewan;
    NamespaceExample.kucing = new Hewan ();
    class Animal {
        name : string;
        constructor () {
            this.name = "Burung Kenari";
        }
    }
    NamespaceExample.Animal = Animal;
} (NamespaceExample || (NamespaceExample = {})));
const kenariBurung : any = new NamespaceExample.Animal ();
console.log (kenariBurung);
const kucingPutih : any = NamespaceExample.kucing;
console.log (kucingPutih);
