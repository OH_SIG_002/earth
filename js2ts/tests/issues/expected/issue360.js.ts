type JSDynamicObject = any;
var v1 : any = Reflect.apply (Math.floor, undefined, [1.75]);
console.log (v1);
function func1 (this : JSDynamicObject, a : number, b : number, c : number) : void {
    this.sum = a + b + c;
}
const args : number [] = [1, 2, 3];
const object1 : any = new func1 (1, 2, 3);
const object2 : any = Reflect.construct (func1, args);
console.log (object2.sum);
console.log (object1.sum);
const object3 : JSDynamicObject = {};
var v2 : boolean = Reflect.defineProperty (object3, 'property1', {
    value : 42
});
if (v2) {
    console.log ('property1 created!');
} else {
    console.log ('problem creating property1');
}
console.log (object3.property2);
const object4 : JSDynamicObject = {
    property1 : 42
};
var v3 : boolean = Reflect.deleteProperty (object4, 'property1');
const array1 : number [] = [1, 2, 3, 4, 5];
Reflect.deleteProperty (array1, '3');
console.log (array1);
const object5 : JSDynamicObject = {
    x : 1,
    y : 2
};
var v4 : any = Reflect.get (object5, 'x');
console.log (v4);
const array2 : string [] = ['zero', 'one'];
console.log (Reflect.get (array2, 1));
const object6 : JSDynamicObject = {
    property1 : 42
};
var v5 : PropertyDescriptor | undefined = Reflect.getOwnPropertyDescriptor (object6, 'property1');
console.log (v5.value);
console.log (Reflect.getOwnPropertyDescriptor (object6, 'property2'));
console.log (Reflect.getOwnPropertyDescriptor (object6, 'property1').writable);
const object7 : JSDynamicObject = {
    property1 : 42
};
const proto1 : object | null = Reflect.getPrototypeOf (object7);
console.log (proto1);
console.log (Reflect.getPrototypeOf (proto1));
var v6 : boolean = Reflect.has (object6, 'property1');
console.log (v6);
console.log (Reflect.has (object1, 'property2'));
var v7 : boolean = Reflect.isExtensible (object3);
console.log (v7);
var v8 : boolean = Reflect.preventExtensions (object3);
console.log (v8);
console.log (Reflect.isExtensible (object3));
const object8 : any = Object.seal ({});
console.log (Reflect.isExtensible (object8));
const object9 : JSDynamicObject = {
    property1 : 42,
    property2 : 13
};
const array3 : any [] = [];
var v9 : PropertyKey [] = Reflect.ownKeys (object9);
console.log (v9);
console.log (Reflect.ownKeys (array3));
console.log (Reflect.setPrototypeOf (object1, Object.prototype));
console.log (Reflect.setPrototypeOf (object1, null));
var v10 : boolean = Reflect.set (object3, 'property1', 42);
console.log (object3.property1);
const array4 : string [] = ['duck', 'duck', 'duck'];
v10 = Reflect.set (array1, 2, 'goose');
console.log (array4 [2]);
var v11 : boolean = Reflect.setPrototypeOf (object3, Object.prototype);
console.log (v11);
v11 = Reflect.setPrototypeOf (object3, null);
console.log (v11);
