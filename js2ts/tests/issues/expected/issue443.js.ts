type JSDynamicObject = any;
function validateArg (args : JSDynamicObject, config : JSDynamicObject) : boolean | string {
    var err ! : string;
    args.validate.forEach (function (tokens : any []) : void {
        var key : any = tokens [0];
        var value : any = args [key];
        switch (tokens [1]) {
            case String :
                if (toString.call (value) !== '[object String]') {
                    err = 'Argument "' + key + '" is not a valid String.';
                }
                break;
            default :
                if (toString.call (value) === '[object global]' && ! tokens [2]) {
                    err = 'Argument "' + key + '" is not defined.';
                }
        }
    });
    if (err) {
        return err;
    }
    return true;
}
;
const args : JSDynamicObject = {
    key : 123,
    value : 'myValue',
    validate : [['key', String], ['value', String]]
};
const config : JSDynamicObject = {};
const result : boolean | string = validateArg (args, config);
console.log (result);
