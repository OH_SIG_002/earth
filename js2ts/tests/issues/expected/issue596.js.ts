type JSDynamicObject = any;
function a (radius : number) : any {
    function clipLine (stream : any) : JSDynamicObject {
        var point0 ! : any;
        return {
            point : function (lambda : any, phi : any) : void {
                var point1 : any [] = [lambda, phi];
                var point2 : number = intersect (point0, point1);
                point2 = intersect (point1, point0);
                point2 = intersect (point0, point1);
                var t : number = intersect (point1, point0, true);
            },
        };
    }
    function intersect (a : any [], b : any [], two ? : boolean) : number {
        return 1;
    }
}
console.log (a (0.7));
