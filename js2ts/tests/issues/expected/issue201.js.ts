class Employee {
    static headcount : number = 0;
    static personcount () : number {
        return headcount;
    }
    constructor (firstName : string, lastName : string, jobTitle : string) {
        Employee.headcount ++;
    }
}
let john : Employee = new Employee ('John', 'Doe', 'Front-end Developer');
let jane : Employee = new Employee ('Jane', 'Doe', 'Back-end Developer');
let headcount : number = Employee.headcount;
console.log (headcount);
let personcount : number = Employee.personcount ();
console.log (personcount);
console.log (headcount == personcount);
