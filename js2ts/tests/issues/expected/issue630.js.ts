export default class Path {
    _x0 : any;
    _y0 ! : any;
    _x1 ! : any;
    _y1 ! : any;
    _ : string;
    constructor () {
        this._x0 = this._y0 = this._x1 = this._y1 = null;
        this._ = "";
    }
    moveTo (x : any, y : any) : void {
        this._ += `M${this._x0 = this._x1 = +x},${this._y0 = this._y1 = +y}`;
    }
    closePath () : void {
        if (this._x1 !== null) {
            this._x1 = this._x0;
            this._y1 = this._y0;
            this._ += "Z";
        }
    }
    lineTo (x : any, y : any) : void {
        this._ += `L${this._x1 = +x},${this._y1 = +y}`;
    }
}
