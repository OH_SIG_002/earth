type JSDynamicObject = any;
"use strict";
import data from "./issue525-2.json";
const descriptionMap : JSDynamicObject = {};
const codeMap : JSDynamicObject = {};
function mapCodeAndDescription (item : any) : void {
    descriptionMap [item.description.toLowerCase ()] = item.code;
    codeMap [item.code] = item.description;
};
data.forEach (mapCodeAndDescription);
export const getCode : any = (description : any) : any => {
        return descriptionMap [description.toLowerCase ()];
    };
export const getCodes : any = () : any => {
        return data.map ((item : any) : any => {
                return item.code;
            });
    };
export const getCodeList : any = () : JSDynamicObject => {
        return codeMap;
    };
export const getDescription : any = (code : any) : any => {
        return codeMap [String (code)];
    };
export const getDescriptions : any = () : any => {
        return data.map ((item : any) : any => {
                return item.description;
            });
    };
export const getDescriptionList : any = () : JSDynamicObject => {
        return descriptionMap;
    };
export const getData : any = () : any => {
        return data;
    };
export const overwrite : any = (sicArray : any) : any => {
        if (! sicArray || ! sicArray.length) return;
        sicArray.forEach ((sic : any) : any => {
                const foundIndex : any = data.findIndex ((item : any) : boolean => {
                        return item.code === String (sic.code);
                    });
                if (foundIndex > - 1) {
                    data [foundIndex] = sic;
                    mapCodeAndDescription (sic);
                }
            });
    }
;
