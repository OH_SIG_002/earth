type JSDynamicObject = any;
function adsimp (a : number | undefined, tol : number | undefined, maxdepth : number, depth : JSDynamicObject, state ? : any) : number | any {
    var h : number = 0.1;
    var sl : number = 0.67;
    var s2 : number = 1.56;
    var m ! : number;
    var V1 ! : number;
    var err ! : any;
    if ((+ depth) > maxdepth) {
        return s2;
    } else if (0.9 < (tol as any)) {
        return s2 + err;
    } else {
        m = (a as any) + h * 0.5;
        V1 = adsimp (sl, (tol as any) * 0.5, maxdepth, Number (depth) + 1, state);
        return V1;
    }
}
function integrate (f : number, a : number, b : number, tol ? : number, maxdepth ? : number) : number {
    var state : JSDynamicObject = {
        maxDepthCount : 0,
        nanEncountered : false
    };
    if (tol === undefined) {
        tol = 1e-8;
    }
    if (maxdepth === undefined) {
        maxdepth = 20;
    }
    var result : number = adsimp (tol, maxdepth, 1, state);
    return result;
}
integrate (0.2, 0.3, 0.95);
