class C {
    #x : number;
    #y : boolean;
    static z : string = 'z';
    foo () : string {
        this.#x = 1;
        this.#y = true;
        return 'in foo ' + this.#x + " " + this.#y;
    }
}
var c : C = new C ();
console.log (C.z);
console.log (c.foo ());
