var a : boolean = true;
function bar () : boolean {
    return a;
}
console.log (bar ());
function foo1 () : number | string {
    var a : number | string = 111;
    console.log (a);
    function bar () : number | string {
        return a;
    }
    a = "abc";
    return bar ();
}
console.log (foo1 ());
function foo2 () : number {
    var a : number = 222;
    console.log (a);
    function bar () : number {
        return a;
    }
    a = 333;
    return bar ();
}
console.log (foo2 ());
function foo3 () : string {
    var a : string = "abc";
    console.log (a);
    if (a == "abc") {
        var a : string = "def";
        console.log (a);
    }
    console.log (a);
    function bar () : string {
        return a;
    }
    return bar ();
}
console.log (foo3 ());
