function broken () : void {
    do var x : number = 42;
    while (false);
    while (false) var x : number = 42;
    if (true) true;
    else var x : number = 42;
    for (; false;) var x : number = 42;
    console.log ("not broken!");
}
broken ();
