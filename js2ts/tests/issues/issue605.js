class a {
    async getData() {
        try {
          const response = await fetch('https://jsonplaceholder.typicode.com/posts/1');
          const data = await response.json();
          return JSON.stringify(data);
        } catch (error) {
          console.error(error);
          throw error
        }
      }
}
var b = new a
console.log(b.getData())
