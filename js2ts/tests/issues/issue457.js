const data = (function () {
    const d = []
    let i = 0
    let k
    for (; i < 10; i++) {
        k = (i < 5 ? '0' : '') + i
        d.push({
            key: k,
            value: String(Math.random())
        })
    }
    return d
}())
console.log(data)
