console.log(Math.E);
// Expected output: 2.718281828459045
function getNatLog10() {
  return Math.LN10;
}
console.log(getNatLog10());

function getNatLog2() {
  return Math.LN2;
}
console.log(getNatLog2());
// Expected output: 0.6931471805599453
function getLog10e() {
  return Math.LOG10E;
}

console.log(getLog10e());
// Expected output: 0.4342944819032518
function getLog2e() {
  return Math.LOG2E;
}

console.log(getLog2e());
// Expected output: 1.4426950408889634
function calculateCircumference(radius) {
  return 2 * Math.PI * radius;
}

console.log(calculateCircumference(1));
// Expected output: 6.283185307179586

console.log(Math.PI);
// Expected output: 3.141592653589793

function getRoot1Over2() {
  return Math.SQRT1_2;
}

console.log(getRoot1Over2());
// Expected output: 0.7071067811865476

function getRoot2() {
  return Math.SQRT2;
}

console.log(getRoot2());
// Expected output: 1.4142135623730951
