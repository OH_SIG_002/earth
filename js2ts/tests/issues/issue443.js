function validateArg (args, config) {
    var err;
  
    args.validate.forEach(function (tokens) {
      var key = tokens[0]
        , value = args[key];
  
      switch(tokens[1]){
        case String:
          if (toString.call(value) !== '[object String]') {
            err = 'Argument "' + key + '" is not a valid String.';
          }
          break;
  
        default:
          if (toString.call(value) === '[object global]' && !tokens[2]) {
            err = 'Argument "' + key + '" is not defined.';
          }
      }
    });
  
    if (err){
      return err;
    }
  
    return true;
  };

  const args = {
    key: 123,
    value: 'myValue',
    validate: [
      ['key', String],
      ['value', String]
    ]
  };
  
  const config = {};
  
  const result = validateArg(args, config);
  console.log(result);
