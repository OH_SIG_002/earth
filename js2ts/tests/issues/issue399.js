function broken() {
    // Every one of the following causes a syntax error!
    do var x = 42; while (false);
    while (false) var x = 42; 
    if (true) true; else var x = 42;
    for (;false;) var x = 42;
    // with (true) var x = 42;        // syntax error, but not allowed in TS
    console.log ("not broken!");
}
broken();
