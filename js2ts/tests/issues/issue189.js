var v1  =  Math.log1p(-2);
console.log(v1);
// Expected output: NaN
console.log(typeof(v1));

var v2  = Math.log2(0);
console.log(v2);
// Expected output: -Infinity
console.log(typeof(v2));

var v3 = Math.cbrt(Infinity);
console.log(v3);
// Expected output: Infinity
console.log(typeof(v3));
