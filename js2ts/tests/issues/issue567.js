function safeMin(x, y) {
    if ((x < y) !== (y < x)) { return y < x ? y : x; }
    return undefined;
  }
console.log(safeMin(1, 2))
