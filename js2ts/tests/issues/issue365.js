function* counter() {
    let count = 0;
    while (true) {
        yield count++;
    }
}
const gen = counter();
var _next = gen.next().value;
console.log(_next); // 0
_next = gen.next().value;
console.log(_next); // 1
console.log(gen.next().value); // 2
var _return = gen.return(3).value;
console.log(_return);
