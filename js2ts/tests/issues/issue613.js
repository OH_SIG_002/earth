function Test (exports_1, context_1) {
    var b2_math_js_1, b2ReferenceFace, b2EPColliderVertexType, b2EPCollider;
    return {
        execute : function () {
            
            b2ReferenceFace = class b2ReferenceFace {
                constructor() {
                    this.i1 = 0;
                    this.i2 = 0;
                    this.sideOffset1 = 0;
                    this.sideOffset2 = 0;
                }
            };
            (function (b2EPColliderVertexType) {
                b2EPColliderVertexType[b2EPColliderVertexType["e_isolated"] = 0] = "e_isolated";
                b2EPColliderVertexType[b2EPColliderVertexType["e_concave"] = 1] = "e_concave";
                b2EPColliderVertexType[b2EPColliderVertexType["e_convex"] = 2] = "e_convex";
            })(b2EPColliderVertexType || (b2EPColliderVertexType = {}));
            
            b2EPCollider.s_rf = new b2ReferenceFace();
        }
    };
}
