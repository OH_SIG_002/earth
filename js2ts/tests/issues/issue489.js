function foo (bucket, keys){
    var self = this;
    if(Array.isArray(keys)){
      return keys.map(function(key){
        return self.prefix+'_'+bucket+'@'+key;
      });
    }else{
      return self.prefix+'_'+bucket+'@'+keys;
    }
  }
  var obj = {
    prefix: 'myPrefix'
  };
  
  var result = foo.call(obj, 'myBucket', ['key1', 'key2']);
  console.log(result);
