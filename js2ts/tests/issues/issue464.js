function _defineProperty(obj, key, value) {
    if (key in obj) {
        Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });
    } else {
        obj[key] = value;
    } return obj;
}
let person = {};
_defineProperty(person, 'name', void 0);
_defineProperty(person, 'age', 21);
console.log(person);
