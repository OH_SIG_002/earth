function A () {   
}

A.prototype = {
    a : 1
}

function B () {
    var b = new A()
    return b.a
}

console.log(B())
