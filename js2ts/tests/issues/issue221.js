// direct
x = "abc";
x = x + 1;
console.log (x);
x = 41;
x = x + 1;
console.log (x);

// as parameter
function f (x) {
    x = x + 1;
    console.log (x);
}

f("abc");
f(41);
