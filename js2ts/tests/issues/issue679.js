export default class Monster {
    _lifePoints;
    _strength;
    constructor() {
        this._lifePoints = 85;
        this._strength = 63;
    }
    get lifePoints() {
        return this._lifePoints;
    }
    get strength() {
        return this._strength;
    }
    receiveDamage(attackPoints) {
        const damage = this.lifePoints - attackPoints;
        if (damage <= 0) {
            this._lifePoints = -1;
            return this.lifePoints;
        }
        this._lifePoints = damage;
        return this.lifePoints;
    }
    attack(enemy) {
        enemy.receiveDamage(this._strength);
    }
}
