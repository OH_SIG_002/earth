// Exporting declarations
export let name1, name2/*, … */; // also var
export const name3 = 1, name4 = 2/*, … */; // also var, let
export function functionName() { /* … */ }
export class ClassName { /* … */ }
export function* generatorFunctionName() {  
  console.log("code before the yield");
  yield 100;
}


let o = "hi";
const cars = ["Volvo", "BMW"];
export const { name5, name6: bar } = o;
export const [ name7, name8 ] = cars;


// Export list
export { n1, /* …, */ nN };
let n1=1;
let nN= 99;
export { variable1 as name01, variable2 as name02 };
let variable1 =11;
let variable2 =22;
//export { variable2 as "string name" }; // /It is a feature in es2022 error was reported https://stackoverflow.com/questions/74726261/how-to-rename-es-module-export-to-a-string-literal-that-is-not-a-valid-identifie
export { name3 as default /*, … */ };

export { myFunction2, myVariable2 };

let myVariable2 = Math.sqrt(2);
function myFunction2() {
  return 78;
  // …
}
// export individual features (can export var, let,
// const, function, class)
export let myVariable = Math.sqrt(2);
export function myFunction() {
  return 8;
  // …
}

export { x };
const x = 1;
// This works, because `export` is only a declaration, but doesn't
// utilize the value of `x`.



