export let name1 : any, name2 : any;
export const name3 : number = 1, name4 : number = 2;
export function functionName () : void {
}
export class ClassName {}
export function * generatorFunctionName () : Generator <number, any, any> {
    console.log ("code before the yield");
    yield 100;
}
let o : string = "hi";
const cars : string [] = ["Volvo", "BMW"];
export const {name5, name6 : bar} : any = o;
export const [name7, name8] : any = cars;
export {n1, nN};
let n1 : number = 1;
let nN : number = 99;
export {variable1 as name01, variable2 as name02};
let variable1 : number = 11;
let variable2 : number = 22;
export {name3 as default};
export {myFunction2, myVariable2};
let myVariable2 : number = Math.sqrt (2);
function myFunction2 () : number {
    return 78;
}
export let myVariable : number = Math.sqrt (2);
export function myFunction () : number {
    return 8;
}
export {x};
const x : number = 1;
