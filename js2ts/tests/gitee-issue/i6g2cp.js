class ParentClass {
  constructor() {}
}

class ChildClass extends ParentClass {
  constructor() {
    super();
  }
}

let p = new ParentClass();
let c = new ChildClass();
console.log(p)
console.log(c)

