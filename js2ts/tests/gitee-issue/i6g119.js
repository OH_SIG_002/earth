console.log(new Number instanceof Number);

var test;
class Test{}
test = new Test;
console.log(test instanceof Test);

function TestType(v) {
    return v instanceof Test;
}

console.log(TestType(test));
console.log(TestType(1));
