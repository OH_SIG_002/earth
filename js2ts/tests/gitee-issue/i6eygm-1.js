/*var fooCalled = false;
function foo1() { 
    fooCalled = true; 
} 
var o1 = {};
o1.bar.gar( foo1() );
*/
var fooCalled = false;
function foo1() {
    console.log("foo1");
    fooCalled = true;
    return fooCalled;
}
var o1 = { bar: (f) => { return f; } };
o1.bar(foo1());
