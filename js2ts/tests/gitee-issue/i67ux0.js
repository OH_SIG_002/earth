let left1 = 1;
let right1 = new Boolean(true);
let result1 = left1 + right1;
console.log(result1);

let left2 = true;
let right2 = new Number(1);
let result2 = left2 + right2;
console.log(result2);

let left3 = new Boolean(true);
let right3 = new Number(1);
let result3 = left3 + right3;
console.log(result3);

let left4 = new String("1");
let right4 = "1";
let result4 = left4 + right4;
console.log(result4);

let left5 = function () { return 1; };
let right5 = function () { return 1; };
let result5 = left5() + right5();
console.log(result5);

let left6 = 2n;
let right6 = 1n;
let result6 = left6 + right6;
console.log(result6);
