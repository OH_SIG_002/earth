const object7 = {
  property1: 42
};

const proto1 = Reflect.getPrototypeOf(object7);

console.log(proto1);
// Expected output: Object {  }

console.log(Reflect.getPrototypeOf(proto1));
// Expected output: null
