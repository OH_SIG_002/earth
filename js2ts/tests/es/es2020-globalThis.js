var getGlobal = function () {
    if (typeof globalThis !== 'undefined') { return globalThis; }
    if (typeof self !== 'undefined') { return self; }
    if (typeof window !== 'undefined') { return window; }
    if (typeof global !== 'undefined') { return global; }
    throw new Error('unable to locate global object');
};

var globals = getGlobal();

if (typeof globals.setTimeout !== 'function') {
    console.log('no setTimeout in this environment or runtime');
}

if (typeof globalThis.setTimeout !== 'function') {
    console.log('no setTimeout in this environment or runtime');
}

console.log(globalThis === globalThis.globalThis); // true (everywhere)
//console.log(window === window.window); // true (in a browser)
//console.log(self === self.self); // true (in a browser or a Web Worker)
//console.log(frames === frames.frames); // true (in a browser)
console.log(global === global.global); // true (in Node.js)
