type JSDynamicObject = any;
let id : symbol = Symbol ("id");
let user : JSDynamicObject = {
    name : "John",
    age : 40,
    [id] : 111
};
for (let key in user) {
    console.log (key);
}
console.log (JSON.stringify (user));
console.log (Object.keys (user));
console.log ("User Id: " + user [id]);
const logLevels : JSDynamicObject = {
    DEBUG : Symbol ('debug'),
    INFO : Symbol ('info'),
    WARN : Symbol ('warn'),
    ERROR : Symbol ('error'),
};
console.log (logLevels.DEBUG, 'debug message');
console.log (logLevels.INFO, 'info message');
console.log (Symbol ('foo') === Symbol ('foo'));
console.log (Symbol.for ('foo') === Symbol.for ('foo'));
