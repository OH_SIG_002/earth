type JSDynamicObject = any;
let typeMap : Map <any, any> = new Map <any, any> ();
var keyObj : JSDynamicObject = {
    'one' : 1
};
typeMap.set ('10', 'string');
typeMap.set (10, 'number');
typeMap.set (true, 'boolean');
typeMap.set (keyObj, 'object');
console.log (typeMap.get (10));
console.log (typeMap.get ('10'));
console.log (typeMap.get (keyObj));
console.log (typeMap.get ({
    'one' : 1
}));
console.log (typeMap.size);
for (let item of typeMap) {
    console.log (item);
}
for (let item in typeMap) {
    console.log (item);
}
