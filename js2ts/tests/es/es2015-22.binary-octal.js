//Binary literals
const num = 0b110;
console.log(num); // 6

//Octal literals
//const num1 = 055;	//SyntaxError: Octal literals are not allowed in strict mode.
//console.log(num1); // 45

//const invalidNum = 028; //SyntaxError: Decimals with leading zeros are not allowed in strict mode.
//console.log(invalidNum); // treated as decimal 28
