import add from './add.js';
import subtract from './subtract.js';
import multiply from './multiply.js';
import divide from './divide.js';
import square from './square.js';
import fraction from './fraction.js';
import factorial from './factorial.js';

function calculate(operation, num1, num2) {
  switch (operation) {
    case 'add':
      return add(num1, num2);
    case 'subtract':
      return subtract(num1, num2);
    case 'multiply':
      return multiply(num1, num2);
    case 'divide':
      return divide(num1, num2);
    case 'square':
      return square(num1);
    case 'fraction':
      return fraction(num1);
    case 'factorial':
      return factorial(num1);
    default:
      throw new Error('Invalid operation');
  }
}

const num1 = 5;
const num2 = 10;

try {
  const resultAdd = calculate('add', num1, num2);
  console.log(`Addition: ${resultAdd}`);

  const resultSubtract = calculate('subtract', num1, num2);
  console.log(`Subtraction: ${resultSubtract}`);

  const resultMultiply = calculate('multiply', num1, num2);
  console.log(`Multiplication: ${resultMultiply}`);

  const resultDivide = calculate('divide', num1, num2);
  console.log(`Division: ${resultDivide}`);

  const resultSquare = calculate('square', num1);
  console.log(`Square: ${resultSquare}`);

  const resultFraction = calculate('fraction', num1);
  console.log(`Fraction: ${resultFraction}`);

  const resultFactorial = calculate('factorial', num1);
  console.log(`Factorial: ${resultFactorial}`);
} catch (error) {
  console.log(`Error: ${error.message}`);
}
