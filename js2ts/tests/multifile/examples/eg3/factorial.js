import multiply from './multiply.js';
  
  function factorial(n) {
    if (n === 0 || n === 1) {
      return 1;
    } else {
      let result = 1;
      for (let i = 2; i <= n; i++) {
        result = multiply(result, i);
      }
      return result;
    }
  }
  
  export default factorial;
  