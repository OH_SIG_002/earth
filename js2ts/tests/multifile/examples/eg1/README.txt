First version of multi-file transformation by js2ts.
To transform a multi-file program, use the js2ts.sh command on the main JS file.

Example:

    For main.js with imported files a.js b.js and c.js, use the command:

        ./js2ts.sh main.js

    Outputs will be in main.ts, a.ts, b.ts and c.ts.

Warning!

    Be careful - "tsc" is destructive! 
    Running "tsc main.ts" on the result will overwrite the original JS files.
