function doA(a) {
    return a;
}
function doB(b) {
    return doA( b + 1 ); //tail call
}
function foo() {
    return 20 + doB(10); //not tail call
}
console.log(foo()); // 31
