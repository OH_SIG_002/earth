function getRectArea (width : number, height : number) : number {
    if (width > 0 && height > 0) {
        return width * height;
    }
    return 0;
}
console.log (getRectArea (3, 4));
console.log (getRectArea (- 3, 4));
