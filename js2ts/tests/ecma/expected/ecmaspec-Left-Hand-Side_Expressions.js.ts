type JSDynamicObject = any;
var i : number = 10;
var total : number = 0;
var obj : JSDynamicObject = {};
obj.x = 10;
let array : any [] = [];
array [0] = 20;
array [1] = 'hello';
var obj2 : JSDynamicObject = {
    Proj : "js2ts",
    Year : 2023
};
console.log (obj2.Proj, obj2.Year);
