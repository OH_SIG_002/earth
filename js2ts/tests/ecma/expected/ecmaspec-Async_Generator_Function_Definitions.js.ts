type JSDynamicObject = any;
const obj4 : JSDynamicObject = {
    f : async function * () : AsyncGenerator <number, any, any> {
        yield 1;
        yield 2;
        yield 3;
    },
};
const obj5 : JSDynamicObject = {
    async * f () : any {
        yield 1;
        yield 2;
        yield 3;
    },
};
