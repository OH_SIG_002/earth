const obj4 = {
    f: async function* () {
      yield 1;
      yield 2;
      yield 3;
    },
  };
 
  // The  object using shorthand syntax
  const obj5 = {
    async *f() {
      yield 1;
      yield 2;
      yield 3;
    },
  };
  