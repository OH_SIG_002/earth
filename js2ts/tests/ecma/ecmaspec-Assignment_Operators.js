let x = 2;
const y = 3;

console.log(x);
// Expected output: 2

console.log(x = y + 1); // 3 + 1
// Expected output: 4

console.log(x = x * y); // 4 * 3
// Expected output: 12

let a = 1;
let b = 0;

a &&= 2;
console.log(a);
// Expected output: 2

b &&= 2;
console.log(b);
// Expected output: 0

const a1 = { duration: 50, title: '' };

a1.duration ||= 10;
console.log(a1.duration);
// Expected output: 50

a1.title ||= 'title is empty.';
console.log(a1.title);
// Expected output: "title is empty"