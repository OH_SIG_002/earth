% JS2TS - convert JavaScript to TypeScript using type inference
% James Cordy, Huawei Technologies
% May 2022 (Rev. Aug 2023)

% Type merging and refinement rules, used in inference rules everywhere

% T1 [mergeType T2] refines type T1 with new information from known type T2

% The following are the type combination rules in order of precedence,
% where T, T1 and T2 are [KnownType]s

%      Type 1     Type 2      Result
%      ------     ------      ------

% Array, Set and Generator types
%       any []      T []        T []
%       T []        any []      T []

% Other types
%       any         T           T
%       T           any         T
%       any         any         any
%       T           T           T

% If all else fails, union
%       T1          T2          T1 | T2

function mergeType Type2 [KnownType]
    % This rule is not used by itself, but rather provided for use by other rule sets

    % mergeType (T1,T2) is used for refinement, not alternation, so union of the types is the last choice.
    % In general, refinement tries to replace 'any' with a better estimate.

    replace [Type]
        Type1 [Type]

    % If T2 is the same type, then we have no new information, so the result is T1
    deconstruct not * [KnownType] Type1
        Type2

    % The order of type combination rules matters, and once one of them has made a result type,
    % we don't want the others to change it.  "Matched" keeps track of whether we already have a result
    export Matched [id]
        'no
    by
        Type1
            % Array type combinations
            [_mergeArrayTypeLeft Type2]
            [_mergeArrayTypeRight Type2]
            [_mergeArrayTypeConflict Type2]

            % Set type combinations
            [_mergeSetTypeLeft Type2]
            [_mergeSetTypeRight Type2]

            % Generator type combinations
            [_mergeGeneratorTypeLeft Type2]
            [_mergeGeneratorTypeRight Type2]

            % Refinements of "any"
            [_mergeAnyTypeLeft Type2]
            [_mergeAnyTypeRight Type2]

            % Function type combinations
            [_mergeFunctionTypeLeft Type2]
            [_mergeFunctionTypeRight Type2]

            % If all else fails, make a union type
            [_mergeUnionType Type2]
end function

% Private

function _mergeArrayTypeLeft Type2 [KnownType]
    % Refine an array of unknown type with an array of known type
    % e.g., any[] | number[] refines to number[]
    import Matched [id]
        'no
    % Do we have an array of unknown type
    replace [Type]
        'any '[ ']
    % And an array of known type
    deconstruct Type2
        PrimaryType [PrimaryType] '[ ']
    export Matched
        'yes
    by
        PrimaryType '[ ']
end function

function _mergeArrayTypeRight Type2 [KnownType]
    % Same as above, but in reverse
    import Matched [id]
        'no
    % Do we have an array of known type
    replace [Type]
        PrimaryType [PrimaryType] '[ ']
    % And an array of unknown type
    deconstruct Type2
        'any '[ ']
    % The we refine to the known type
    export Matched
        'yes
    by
        PrimaryType '[ ']
end function

function _mergeArrayTypeConflict Type2 [KnownType]
    % If we are combining two array types of different element types,
    % it's better in most cases to refine to any[] rather than a union element type
    import Matched [id]
        'no
    % Do we have two arrays of different known types?
    replace [Type]
        PrimaryType1 [PrimaryType] '[ ']
    deconstruct Type2
        PrimaryType2 [PrimaryType] '[ ']
    % If they're the same type, we're already done
    deconstruct not PrimaryType1
        PrimaryType2
    % If not, we refine to any[]
    export Matched
        'yes
    by
        'any '[ ']
end function

function _mergeSetTypeLeft Type2 [KnownType]
    % Refine a Set of unknown type with a Set of known type
    % e.g., Set<any> | Set<number> refines to Set<number>
    import Matched [id]
        'no
    % Do we have a Set of unknown type
    replace [Type]
        'Set '< 'any '>
    % And a set of known type
    deconstruct Type2
        'Set '< Type [Type] '>
    % The we refine to the known type
    export Matched
        'yes
    by
        'Set '< Type '>
end function

function _mergeSetTypeRight Type2 [KnownType]
    % Same as above in reverse
    import Matched [id]
        'no
    % Do we have a Set of known type
    replace [Type]
        'Set '< Type [Type] '>
    % And a Set of unknown type
    deconstruct Type2
        'Set '< 'any '>
    % Then we refine to the known type
    export Matched
        'yes
    by
        'Set '< Type '>
end function

function _mergeGeneratorTypeLeft Type2 [KnownType]
    % Refine a Generator of unknown type with a Generator of known type
    % e.g., Generator<any,any,any> | Generator<number,any,any> refines to Generator<number,any,any>
    import Matched [id]
        'no
    % Do we have a Generator of unknown type
    replace [Type]
        'Generator '< 'any , 'any , 'any '>
    % And a Generator of known type
    deconstruct Type2
        'Generator '< Type [Type] , 'any , 'any '>
    % Then we refine to the known type
    export Matched
        'yes
    by
        'Generator '< Type , 'any , 'any '>
end function

function _mergeGeneratorTypeRight Type2 [KnownType]
    % Same as above, but in reverse
    import Matched [id]
        'no
    % Do we have a Generator of known type
    replace [Type]
        'Generator '< Type [Type] , 'any , 'any '>
    % And a Generator of unknown type
    deconstruct Type2
        'Generator '< 'any , 'any , 'any '>
    % Then we refine to the known type
    export Matched
        'yes
    by
        'Generator '< Type , 'any , 'any '>
end function

function _mergeFunctionTypeLeft Type2 [KnownType]
    % When merging function types, we have to parenthesize them if they are to be a union
    % e.g., mergeing (x) => T1 and (y) => T2 can only be ((x) => T1) | ((y) => T2)
    import Matched [id]
        'no
    % If we have a function type
    replace [Type]
        FunctionType [FunctionType]
    % And another type that's not the same
    deconstruct not Type2
        FunctionType
    % Then parenthesize the function type
    construct ConvertedType [Type]
        '( FunctionType ')
    % And union it with the other type
    construct ResultType [Type]
        ConvertedType [_mergeUnionType Type2]
    export Matched
        'yes
    by
        ResultType
end function

function _mergeFunctionTypeRight Type2 [KnownType]
    % Same as above, but in reverse
    import Matched [id]
        'no
    % If we're merging a type
    replace [Type]
        Type1 [Type]
    % With a function type that's not the same
    deconstruct Type2
        FunctionType [FunctionType]
    deconstruct not Type1
        FunctionType
    % Then parenthesize the function type
    construct ConvertedType [KnownType]
        '( FunctionType ')
    % And union it with the other type
    construct ResultType [Type]
        Type1 [_mergeUnionType ConvertedType]
    export Matched
        'yes
    by
        ResultType
end function

function _mergeAnyTypeLeft Type2 [KnownType]
    % If we have an unknown type
    import Matched [id]
        'no
    replace [Type]
        'any
    % And a known type that's not simply undefined
    deconstruct not Type2
        'any
    deconstruct not Type2
        'undefined
    % Then refine to the known type
    export Matched
        'yes
    by
        Type2
end function

function _mergeAnyTypeRight Type2 [KnownType]
    % Same as above, but in reverse
    import Matched [id]
        'no
    % If we have a known type that's not simply undefined
    replace [Type]
        Type1 [Type]
    deconstruct not Type1
        'any
    deconstruct not Type1
        'undefined
    % And an unknown type
    deconstruct Type2
        'any
    % Then refine to the known type
    export Matched
        'yes
    by
        Type1
end function

function _mergeUnionType Type2 [KnownType]
    % If all else fails, try to make a union of the types
    import Matched [id]
        'no
    % We may already have union type(s)
    replace [Type]
        Type1 [UnionType]
    % Extract all the types in the union(s), or just the types themselves
    construct Types12 [IntersectionType*]
        _ [^ Type1] [^ Type2]
    % Remove any duplicates in the set
    construct Types [IntersectionType*]
        _ [_removeDuplicateTypes each Types12]
    % The grammar requires that the first type in a union be [UnionType], even if a singleton
    deconstruct Types
        FirstType [IntersectionType] MoreTypes [IntersectionType*]
    construct ResultType [UnionType]
        FirstType
    % Create a union of all the types
    by
        ResultType [_unionType each MoreTypes]
                   [_notNullUndefined]
end function

function _removeDuplicateTypes NextType [IntersectionType]
    % If the next type isn't a duplicate, add it to the set
    replace [IntersectionType*]
        Types [IntersectionType*]
    deconstruct not * [IntersectionType] Types
        NextType
    % Array unions may be refinements
    where not
        Types [_hasRefinedArrayType NextType]
    by
        Types [. NextType]
end function

function _hasRefinedArrayType NextType [IntersectionType]
    % Is the next type is an array of unknown element type
    deconstruct NextType
        'any '[ ']
    % And we already have a refinement in the union?
    match * [IntersectionType]
        _ [PrimaryType] '[ ']
end function

function _unionType Type2 [IntersectionType]
    % Add another type to a union
    replace [UnionType]
        Type1 [UnionType]
    by
        Type1 [_unionArrayType Type2]
              [_unionOtherType Type2]
end function

function _unionArrayType Type2 [IntersectionType]
    % Is the type a refinement of an unknown array type already in the union?
    deconstruct Type2
        PrimaryType [PrimaryType] '[ ']
    % Then refine the unknown rather than creating a new alternative
    replace * [IntersectionType]
        'any '[ ']
    by
        PrimaryType '[ ']
end function

function _unionOtherType Type2 [IntersectionType]
    % Otherwise, create a new alternative of the union
    replace [UnionType]
        Type1 [UnionType]
    % Don't union with "any"
    deconstruct not Type1
        'any
    deconstruct not Type2
        'any
    % Don't union with JSDynamicObject
    deconstruct not Type1
        'JSDynamicObject
    % Don't refine built-in typed array types
    where not
        Type1 [_isBuiltinArrayType]
    % Don't repeat types in the union
    deconstruct not * [IntersectionType] Type1
        Type2
    by
        Type1 '| Type2
end function

function _isBuiltinArrayType
    % Is the type a built-in typed array type?
    construct BuiltinArrayTypes [Identifier*]
        'Int8Array 'Uint8Array 'Uint8ClampedArray 'Int16Array 'Uint16Array 'Int32Array
        'Uint32Array 'Float32Array 'Float64Array 'BigInt64Array 'BigUint64Array
    match [UnionType]
        TypeName [Identifier]
    deconstruct * [Identifier] BuiltinArrayTypes
        TypeName
end function

function _notNullUndefined
    % A union of "null" with "undefined" isn't meaningful - "any" is better in that case
    replace [UnionType]
        'null '| 'undefined
    by
        'any
end function
