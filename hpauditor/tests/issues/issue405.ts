//WifiController.ets
const TAG = 'Model.WifiController';

export class WifiController extends BaseController<WifiData> {
  private currentWifiStatus: boolean;
  private timerId: number;

  mWifiData: WifiData = {
    ...new WifiData()
  };

  constructor() {
    super();
    this.onStart();
  }
  //...
}

let sWifiController = SingletonHelper.getInstance(WifiController, TAG);

export default sWifiController as WifiController;
