/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue325.ts */
"use strict";
function foo(cellAndSpan: CellAndSpan): number[] {
  let result: [number, number];
  let resultSpan: [number, number];
  this.lazyInitTempRectStack();
  /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 6 : issue325.ts */
  const pixelX: number = cellAndSpan.getCellX();
  /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 7 : issue325.ts */
  const pixelY: number = cellAndSpan.getCellY();
  let bestDistance: number = Number.MAX_VALUE;
  /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 10 : issue325.ts */
  const bestRect: RectItem = new RectItem(- 1, - 1, - 1, - 1);
  const validRegions: Stack<RectItem> = new Stack<RectItem>();
  for (let i = 0; i < this.mRow - cellAndSpan.getSpanY() + 1; i++) {
    /* HPAudit: Do not access a const property in a heavy loop : hp-performance-no-const-prop-in-heavy-loop : 1 : 13 : issue325.ts */
    const T1 = i - 1;
    for (let j = 0; j < this.mColumn - cellAndSpan.getSpanX() + 1; j++) {
      let cellCoords: number[];
      cellCoords = this.cellToCenterPoint(j, i, cellAndSpan.getSpanX(), cellAndSpan.getSpanY());
      /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 17 : issue325.ts */
      const currentRect: RectItem = this.mTempRectStack.pop();
      if (! currentRect) {
        continue;
      }
      currentRect.set(j, i, j - 1, T1);
      /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 22 : issue325.ts */
      const isContained: boolean = this.isContained4FindNearestArea(validRegions, currentRect);
      validRegions.push(currentRect);
      /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 24 : issue325.ts */
      const distance: number = Math.hypot(cellCoords[0] - pixelX, cellCoords[1] - pixelY);
      if ((distance <= bestDistance && ! isContained) || currentRect.containsRect(bestRect)) {
        bestDistance = distance;
        result = [j, i];
        if (resultSpan != null) {
          resultSpan = [- 1, - 1];
        }
        bestRect.setRect(currentRect);
      }
    }
  }
  if (bestDistance === Number.MAX_VALUE) {
    result = [- 1, - 1];
  }
  this.recycleTempRects(validRegions);
  return result;
}
