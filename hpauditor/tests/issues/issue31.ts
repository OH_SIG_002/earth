// Errors would not be thrown for the following examples
// 1. Assigning to undeclared variables.
let mistypedVariable;
mistypedVarible = 10; // ReferenceError


// 2. Failing to assign to object properties.
// 2.1 Assignment to a non-writable global.
undefined = 5; // TypeError
Infinity = 5; // TypeError

// 2.2 Assignment to non-writable property.
const obj1 = {};
Object.defineProperty(obj1, "x", { value: 17, writable: false });
obj1.x = 10; // TypeError

// 2.3 Assignment to a getter-only property.
const obj2 = {
  get x() {
    return 17;
  }
}
obj2.x = 10; // TypeError

// 2.4 Assignment to a new property on a non-extensible object.
const fixedObj = {};
Object.preventExtensions(fixedObj);
fixedObj.newProp = 'hello'; // TypeError


// 3. Failing to delete object properties.
// 3.1 Deleting a non-configurable.
delete Object.prototype; // TypeError
delete [].length; // TypeError

// 3.2 Deleting plain names.
let x;
delete x; // SyntaxError


// 4. Duplicate parameter names.
function sumOf(num1, num1, num2) {
  // SyntaxError
  return num1 + num1 + num2; // Wrong if code ran.
}
