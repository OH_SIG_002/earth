// What we can and can't detect and fix

// Some of these output NOT OK in the original TS code here
// Once fixed by TSaudit, they should all output OK

const f1 = 0.21;
const f2 = 0.1;
const f3 = f1 + f2;
const f4 = 0.11;

// easy to detect: equality with float literal
if (f1 - 0.1 !== 0.11) {
    console.log ('NOT OK');
} else {
    console.log ('OK');
}

// easy to detect: equality with float literal
if (f1 - f2 === 0.11) {
    console.log ('OK');
} else {
    console.log ('NOT OK');
}

// can detect: equality with float initialized variable
if (f3 - f2 != f1) {
    console.log ('NOT OK');
} else {
    console.log ('OK');
}

const i1 = 1;
const i2 = 2;

// can detect: do not flag, integer comparison
if (i2 - i1 !== 1) {
    console.log ('NOT OK');
} else {
    console.log ('OK');
}

// can detect: should be flagged, float comparison
if (i2 - i1 == 1.0) {
    console.log ('OK');
} else {
    console.log ('NOT OK');
}

const a1 = 1;
const a2 = 3;

// can detect: do not flag, integer comparison
if (a2 - a1 !== a1 + a1) {
    console.log ('NOT OK');
} else {
    console.log ('OK');
}

let a3 = 3;
a3 = 3.0;

// can detect: should be flagged, float comparison
if (a3 - a1 !== a1 + a1) {
    console.log ('NOT OK');
} else {
    console.log ('OK');
}

// can detect: should be flagged, float comparison
if (i1 / i2 === i1 / 2) {
    console.log ('OK');
} else {
    console.log ('NOT OK');
}

let a4 = i1 / i2;   // 0.5
a4 = a4 * 3;        // 1.5

// possibly can not detect (but not in general): should be flagged, float comparison
if (a4 * 2 !== a2) {
    console.log ('NOT OK');
} else {
    console.log ('OK');
}

// However, in general we can't detect all of them, and some will seem too eager, 
// for example, this one will be flagged, even though the computation is integer
if (i2 / 2 !== i1) {
    console.log ('NOT OK');
} else {
    console.log ('OK');
}
