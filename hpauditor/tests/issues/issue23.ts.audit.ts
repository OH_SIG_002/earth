/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue23.ts */
"use strict";
// Declare unchanged variables as const
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 2 : issue23.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 2 : issue23.ts */
const NUM1: number = 10000;
let num2: number = 0;
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 5 : issue23.ts */
function getNum1(): number {
  return NUM1;
}
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 8 : issue23.ts */
function addOne(): number {
  num2 += 1;
  return num2;
}
console.log(getNum1());
console.log(addOne());
