// Arguments must match method parameters
class C {
    static staticf (x: number, y: string) : void {
        console.log (y + x);
    }
    dynamicf (x: boolean, n: number) : void {
        if (x) {
            console.log (n);
        } else {
            staticf (42, '42 is ');
            staticf ();         // wrong number of arguments
        }
    }
}

// Static methods
C.staticf (42, '42 is ');       // ok
C.staticf ('42 is not ', 42);   // wrong types
C.staticf (42);                 // wrong number of arguments
C.staticf ();                   // wrong number of arguments

// Dynamic methods
const o = new C ();
o.dynamicf (true, 42);          // ok
o.dynamicf (42, false);         // wrong types
o.dynamicf (true);              // wrong number of arguments
o.dynamicf ();                  // wrong number of arguments

