/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue245.ts */
"use strict";
// should be no space before <number>
const x: Array<number> = [1, 2, 3]
// should be no space before ++
for (let i = 0; i < 100; i++) {
}
