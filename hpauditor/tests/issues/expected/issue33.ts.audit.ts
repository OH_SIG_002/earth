/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue33.ts */
"use strict";
// Note: with() is unsupported in strict mode, compiler will emit error
const foo: { 
    x: number
  } = { 
    x: 5
  };
/* HPAudit: Do not use with() : hp-specs-no-with : 1 : 3 : issue33.ts */
with (foo) {
  /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 4 : issue33.ts */
  /* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 4 : issue33.ts */
  const X: number = 3;
  console.log(X); // output 3
}
console.log(foo.x);
