/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue413.ts */
"use strict";
//AppModel.ets
export enum APP_STATE { NOT_DOWNLOAD, // 未下载
  DOWNLOADING, // 下载中
  PAUSED, // 暂停中
  WAITING, // 等待下载中
  INSTALLING, // 安装中
  OPEN, // 已安装，显示open
  UPDATE, // 已安装，显示update
}
@Observed
export class AppItem {
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 14 : issue413.ts */
  title ?: string = '';
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 15 : issue413.ts */
  image ?: Resource | image.PixelMap | string | null;
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 16 : issue413.ts */
  iconId ?: number = 0;
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 17 : issue413.ts */
  labelId ?: number = 0;
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 18 : issue413.ts */
  appState ?: APP_STATE;
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 19 : issue413.ts */
  category ?: string = '';
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 20 : issue413.ts */
  desc ?: string = '';
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 21 : issue413.ts */
  progress ?: number = 0;
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 22 : issue413.ts */
  bundleName ?: string = '';
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 23 : issue413.ts */
  moduleName ?: string = '';
}
