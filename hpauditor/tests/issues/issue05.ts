// Do not use float number as the value for textsize and coordinates

{
	"command": "DrawTextBlob",
	"visible":true,
	"x" : 74.5078,     // float number
	"y" : 48.1724,    // float number
	"bounds": [
		-23.1483, 
		-44.6069, 
		139.339, 
		11.4474
	], 
	"paint":{
		"color": [
			255, 
			255, 
			255, 
			255
		]
	},
	"runs":[ 
	  {
		"font":{
			"subpixelText":true,
			"textSize" : 42.2414,   //float number
			"edging": "antialias",
			"hinting": "slight", 
			"typeface": { 
				"data": "/data/0" 
			}
		}
	  }
	]
}
