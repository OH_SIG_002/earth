/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue72.ts */
"use strict";
// 1. There is no space before and after 'void'
// There are spaces in ${ name }
function sayHi(name1: string, name2: string, name3: string): void {
  console.log(`Hi, ${name1}!`);
  console.log(`Hi, ${name2}!`);
  console.log(`Hi, ${name3}!`);
}

// 2. There is a space between fight and (
function fight(): void {
  console.log('Swoosh!');
}

// 3. There is no space between if and (
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 15 : issue72.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 15 : issue72.ts */
const IS_JEDI: boolean = true;
if (IS_JEDI) {
  fight();
}
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 20 : issue72.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 20 : issue72.ts */
const IS_MORNING: boolean = false;
if (IS_MORNING) {
  console.log('Good morning!');
} else {
  // there is no space between else and }
  console.log('Good night!');
}

// 4. There are spaces before the first element/parameter, no spaces after the comma,

// or spaces before the comma, making it difficult to read.
const arr: number[] = [1, 1, 1, 1, 1];
const nameArr: { 
    name: string
  }[] = [{ 
      name: 'John'
    }, { 
      name: 'Amy'
    }, { 
      name: 'Sam'
    }];
sayHi('John', 'Amy', 'Sam');

// 5. There are no spaces before and after binary and ternary operators
if (0 === 0) {
  console.log('Zero!');
}
console.log(1 + 2 === 3);
console.log(IS_JEDI || IS_MORNING);
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 41 : issue72.ts */
const AGE: number = 7;
const price: string = AGE >= 3 ? '$20' : 'Free';
console.log(price);
