/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue308.ts */
"use strict";
class O1 {
  /* HPAudit: Do not delete properties of an object : hp-performance-no-delete-objs-props : 1 : 2 : issue308.ts */
  x: string | null = "";
  y: string | undefined = "";
}
let obj: O1 = { 
    x: "",
    y: ""
  };
obj.x = "xxx";
obj.y = "yyy";
/* HPAudit: Do not delete properties of an object : hp-performance-no-delete-objs-props : 1 : 9 : issue308.ts */
obj.x = null;
class O2 {
  a: string | undefined = "";
  b: string | undefined = "";
}
let obj2: O2 = { 
    a: "",
    b: ""
  };
obj2.a = "xxx";
obj2.b = "yyy";
