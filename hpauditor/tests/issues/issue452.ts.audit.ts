/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue452.ts */
"use strict";
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 1 : issue452.ts */
const regExp = /^\s\([1-9][0-9]*\)$/
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 2 : issue452.ts */
const dupNumSet = new Set<number>()
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 3 : issue452.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 3 : issue452.ts */
const IS_DONE = false
