// Use lowerCamelCase for variables, functions, parameters

// Variable
let FirstMsg: string = "";
FirstMsg = "Hello World";
let playername: string = "";
playername = "John";
let this_variable: number = 0;
this_variable = 42;

// Function
function SENDMSG(Msg: string): void {
  console.log (Msg);
}

function Alertuser(PlayerName: string): void {
  console.log ("Alert: " + PlayerName);
}

SENDMSG(FirstMsg);
Alertuser("Tony");

// Function member
class C {
    name: string = "";
    constructor (Name: string) {
        this.name = Name;
    }
    sayHiTo(Hi: string) {
       console.log(Hi + this.name);
    }
}

const o = new C("Sajeda");
o.sayHiTo("Hi ");
