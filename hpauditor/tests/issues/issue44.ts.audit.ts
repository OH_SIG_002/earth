/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue44.ts */
"use strict";
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 1 : issue44.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 1 : issue44.ts */
const AGE: number = 17;
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 2 : issue44.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 2 : issue44.ts */
const BEE: number = 20;
/* HPAudit: Use strict equality operators : hp-specs-strict-equality-ops : 1 : 3 : issue44.ts */
if (AGE === BEE) {
  console.log(AGE);
}
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 6 : issue44.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 6 : issue44.ts */
const FOO: boolean = true;
/* HPAudit: Use strict equality operators : hp-specs-strict-equality-ops : 1 : 7 : issue44.ts */
if (FOO === true) {
  console.log(FOO);
}
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 10 : issue44.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 10 : issue44.ts */
const BANANAS: number = 2;
/* HPAudit: Use strict equality operators : hp-specs-strict-equality-ops : 1 : 11 : issue44.ts */
if (BANANAS !== 1) {
  console.log(BANANAS);
}
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 14 : issue44.ts */
const value: undefined = undefined;
/* HPAudit: Use strict equality operators : hp-specs-strict-equality-ops : 1 : 15 : issue44.ts */
if (value === undefined) {
  console.log(value);
}
/* HPAudit: Use strict equality operators : hp-specs-strict-equality-ops : 1 : 18 : issue44.ts */
if (typeof FOO === 'undefined') {
  console.log(FOO);
}
/* HPAudit: Use strict equality operators : hp-specs-strict-equality-ops : 1 : 21 : issue44.ts */
console.log('hello' !== 'world');
/* HPAudit: Use strict equality operators : hp-specs-strict-equality-ops : 1 : 22 : issue44.ts */
console.log(0 === 0)
/* HPAudit: Use strict equality operators : hp-specs-strict-equality-ops : 1 : 23 : issue44.ts */
console.log(true === true);
