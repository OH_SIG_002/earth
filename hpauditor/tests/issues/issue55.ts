// Force null and undefined as independent type annotations
// (Do not assign null or undefined unless type includes them)
let userName: string;
userName = 'hello';
userName = undefined;

let num: number;
num = 17;
num = null;
