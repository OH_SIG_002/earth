@Entry
@Component
struct ImageExample1 {
  build() {
    Column() {
        Image('resources/base/media/sss001.jpg')
          .border({ width: 1 }).borderStyle(BorderStyle.Dashed).aspectRatio(1).width('25%').height('12.5%').syncLoad(true)

    // 此处省略若干个Row容器，每个容器内都包含如上的若干Image组件
    }
  }
}