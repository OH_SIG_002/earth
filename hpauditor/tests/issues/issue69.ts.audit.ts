/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue69.ts */
"use strict";
// Without braces, only the first statement after the conditional statement and loop belongs to it.
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 2 : issue69.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 2 : issue69.ts */
const IS_GOOD: boolean = true;
/* HPAudit: Always use braces for conditional statements and loops : hp-specs-use-braces-stmts-loops : 1 : 3 : issue69.ts */
if (IS_GOOD) {
  console.log('success');
}
/* HPAudit: Always use braces for conditional statements and loops : hp-specs-use-braces-stmts-loops : 1 : 6 : issue69.ts */
for (let idx = 0; idx < 5; idx++) {
  console.log(idx);
}
let i = 0;
/* HPAudit: Always use braces for conditional statements and loops : hp-specs-use-braces-stmts-loops : 1 : 10 : issue69.ts */
while (i < 5) {
  i++;
}
