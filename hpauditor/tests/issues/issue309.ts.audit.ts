/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue309.ts */
"use strict";
class C {}
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 3 : issue309.ts */
function foo(c: C): void {
  if (c == null) {
  }
}
