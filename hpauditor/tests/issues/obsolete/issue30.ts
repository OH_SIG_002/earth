// This is a correct example because global variables are stored as local variables,
// thus reducing global lookups.
var twoNums: { num1: number, num2: number };
globalThis.twoNums = { num1: 1, num2: 2 };

function search() {
  let twoNums = globalThis.twoNums;
  console.log(twoNums.num1 + twoNums.num2);
}
search(); // 3
