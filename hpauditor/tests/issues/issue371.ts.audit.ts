/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue371.ts */
"use strict";
/* HPAudit: Use UpperCamelCase for class, enum and namespace names : hp-specs-ucamelcase-cls-enums-ns : 1 : 1 : issue371.ts */
class MyClass {
  /* HPAudit: Use lowerCamelCase for parameter names : hp-specs-lcamelcase-vars-funcs-params : 1 : 2 : issue371.ts */
  constructor(public myProperty: string) {
  }
}
/* HPAudit: Use UpperCamelCase for class, enum and namespace names : hp-specs-ucamelcase-cls-enums-ns : 1 : 5 : issue371.ts */
enum MyEnum { VALUE1, VALUE2, VALUE3 }
/* HPAudit: Use UpperCamelCase for class, enum and namespace names : hp-specs-ucamelcase-cls-enums-ns : 1 : 10 : issue371.ts */
namespace MyNamespace {
  export class MyClass {}
}
/* HPAudit: Use UpperCamelCase for class, enum and namespace names : hp-specs-ucamelcase-cls-enums-ns : 1 : 13 : issue371.ts */
class Myclass {
  /* HPAudit: Use lowerCamelCase for parameter names : hp-specs-lcamelcase-vars-funcs-params : 1 : 14 : issue371.ts */
  constructor(public myProperty: string) {
  }
}
/* HPAudit: Use UpperCamelCase for class, enum and namespace names : hp-specs-ucamelcase-cls-enums-ns : 1 : 17 : issue371.ts */
enum Myenum { VALUE1, VALUE2, VALUE3 }
/* HPAudit: Use UpperCamelCase for class, enum and namespace names : hp-specs-ucamelcase-cls-enums-ns : 1 : 22 : issue371.ts */
namespace Mynamespace {
  export class Myclass {}
}
