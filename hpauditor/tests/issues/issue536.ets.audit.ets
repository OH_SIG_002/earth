// Only report hp-arkui-limit-refresh-scope when there are more than 
// 4 components in the components that contain the if block

// for example, this should not trigger hp-arkui-limit-refresh-scope:
@Component
export struct a1 {
  @State s: Boolean = false
  build() {
    Row() {
      if (this.s) {
        Text('1')
      }
      Text('2')
    }
  }
}

// but this should trigger it:
@Component
export struct a2 {
  @State s: Boolean = false
  build() {
    Row() {
      /* HPAudit: Limit refresh scope using containers : hp-arkui-limit-refresh-scope : 1 : 26 : issue536.ets */
      Stack() {
        if (this.s) {
          Text('1')
        }
      }
      Text('2')
      Text('3')
      Text('4')
      Text('5')
    }
  }
}

// not clear what each of these should do:


// Does the if count as a component? (yes)
@Component
export struct a3 {
  @State s: Boolean = false
  build() {
    Row() {
      /* HPAudit: Limit refresh scope using containers : hp-arkui-limit-refresh-scope : 1 : 45 : issue536.ets */
      Stack() {
        if (this.s) {
          Text('1')
        }
      }
      Text('2')
      Text('3')
      /* HPAudit: Limit refresh scope using containers : hp-arkui-limit-refresh-scope : 1 : 50 : issue536.ets */
      Stack() {
        if (this.s) {
          Text('4')
        }
      }
      Text('5')
    }
  }
}

// Does a ForEach count as a component? (yes)
@Component
export struct a4 {
  @State s: Boolean = false
  build() {
    Row() {
      /* HPAudit: Limit refresh scope using containers : hp-arkui-limit-refresh-scope : 1 : 64 : issue536.ets */
      Stack() {
        if (this.s) {
          Text('1')
        }
      }
      Text('2')
      Text('3')
      Foreach(this.that, (item: gridData, index) => {
        Text('4')
      })
      Text('5')
    }
  }
}

// Do components inside the if and else count as components? (yes)
@Component
export struct a5 {
  @State s: Boolean = false
  build() {
    Row() {
      /* HPAudit: Limit refresh scope using containers : hp-arkui-limit-refresh-scope : 1 : 83 : issue536.ets */
      Stack() {
        if (this.s) {
          Text('1')
        } else {
          Text('2')
          Text('3')
        }
      }
      Text('4')
      Text('5')
    }
  }
}

// Do components inside components count as components? (no)
@Component
export struct a6 {
  @State s: Boolean = false
  build() {
    Row() {
      if (this.s) {
        Text('1')
      }
      Column() {
        Text('2')
      }
        .width('100%');
      Text('3')
      Column() {
        Text('4')
        Text('5')
      }
        .width('100%');
    }
  }
}
