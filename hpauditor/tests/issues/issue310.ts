//310.ts
enum TransitionEffect{
  ONE,
  TWO
}
function  getIconFallTransitionEffect(): TransitionEffect{
  return TransitionEffect.ONE;
}
function  getTransitionEffect(para: string): TransitionEffect {
  switch(para) {
    case 'fall':
      return getIconFallTransitionEffect();
    default:
      break;
  }
  return TransitionEffect.TWO;
}
console.log(getTransitionEffect('fall'))
console.log(getTransitionEffect('spring'))
