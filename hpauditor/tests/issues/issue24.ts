// Avoid using closures
// Pass as parameters instead

// Example 1
const arr: number[] = [1, 2, 3];
function foo(): number {
  return arr[0] + arr[1];
}
console.log(foo());

// Example 2
function greet(): void {
  let greeting: string = "Hi";
  function sayGreeting(): void {
    console.log(greeting);
  }
  sayGreeting();
}
greet();
