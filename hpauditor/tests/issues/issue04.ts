
// Do not access a const property in a heavy loop
function getDay(year: number): number {
	/* Year has (12 * 29 =) 348 days at least */
	let totalDays: number = 348;
	for (let index: number = 0x8000; index > 0x8; index >>= 1) {
		//The INFO and START fields of Time are searched for multiple times, and the found values are the same each time.
		totalDays += ((Time.INFO[year - Time.START] & index) !== 0) ? 1 : 0;
	}
	return totalDays + this.getDays(year);
}
