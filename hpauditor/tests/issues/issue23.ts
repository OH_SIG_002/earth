// Declare unchanged variables as const
let num1: number = 10000;
let num2: number = 0;

function getNum1() {
  return num1;
}
function addOne() {
  num2 += 1;
  return num2;
}
console.log(getNum1());
console.log(addOne());
