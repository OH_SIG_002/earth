/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue64.ts */
"use strict";
// Use lowerCamelCase for variables, functions, parameters

// Variable
/* HPAudit: Use lowerCamelCase for variable and function names : hp-specs-lcamelcase-vars-funcs-params : 1 : 4 : issue64.ts */
let firstMsg: string = "";
firstMsg = "Hello World";
let playername: string = "";
playername = "John";
/* HPAudit: Use lowerCamelCase for variable and function names : hp-specs-lcamelcase-vars-funcs-params : 1 : 8 : issue64.ts */
let thisVariable: number = 0;
thisVariable = 42;

// Function
/* HPAudit: Use lowerCamelCase for variable and function names : hp-specs-lcamelcase-vars-funcs-params : 1 : 12 : issue64.ts */
/* HPAudit: Use lowerCamelCase for parameter names : hp-specs-lcamelcase-vars-funcs-params : 1 : 12 : issue64.ts */
function sendmsg(msg: string): void {
  console.log(msg);
}
/* HPAudit: Use lowerCamelCase for variable and function names : hp-specs-lcamelcase-vars-funcs-params : 1 : 16 : issue64.ts */
/* HPAudit: Use lowerCamelCase for parameter names : hp-specs-lcamelcase-vars-funcs-params : 1 : 16 : issue64.ts */
function alertuser(playerName: string): void {
  console.log("Alert: " + playerName);
}
sendmsg(firstMsg);
alertuser("Tony");

// Function member
class C {
  name: string = "";
  /* HPAudit: Use lowerCamelCase for parameter names : hp-specs-lcamelcase-vars-funcs-params : 1 : 26 : issue64.ts */
  constructor(name: string) {
    this.name = name;
  }
  /* HPAudit: Use lowerCamelCase for parameter names : hp-specs-lcamelcase-vars-funcs-params : 1 : 29 : issue64.ts */
  /* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 29 : issue64.ts */
  sayHiTo(hi: string): void {
    console.log(hi + this.name);
  }
}
const o = new C("Sajeda");
o.sayHiTo("Hi ");
