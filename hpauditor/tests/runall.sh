#!/bin/bash
ulimit -s hard

# Check argument
if [ "$1" = "" ]; then 
    echo "Usage:  runall DIR"
    echo "  (where DIR is a directory tree containing TS/ETS files named using .[e]ts)"

    exit 99
fi

# Compile tsaudit
txlc txl/tsaudit.txl

# Find TS/ETS files (only those named *.[e]ts)
echo -n > ${1}.runout
n=0
f=0

IFS=$'\n'
for i in `find ${1} -type f | egrep -e '\.e?ts$'`
do
    echo "=== $i ===" >> ${1}.runout
    (time tsaudit.x $2 $3 -q $i) >> ${1}.runout 2>&1
    if [ $? != 0 ]; then
        f=`expr $f + 1`
    else
        (time tsaudit.x $2 $3 -q $i -o /dev/null) >> ${1}.runout 2>&1
        if [ $? != 0 ]; then
            f=`expr $f + 1`
        fi
    fi
    n=`expr $n + 1`
    echo -n "$n files, $f failed"
done

# Count results
echo ""
p=`expr $f \* 100000 / $n`
pi=`expr $p / 1000`
pd=`expr 100000 + $p - $pi \* 1000 | cut -c4-6`
echo "$pi.$pd% failed"
echo "output in ${1}.runout"
