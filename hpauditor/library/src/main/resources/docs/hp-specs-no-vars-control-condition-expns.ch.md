## 不要在控制性条件表达式中执行赋值操作


控制性条件表达式常用于if、while、for、?:等条件判断中。
在控制性条件表达式中执行赋值，常常导致意料之外的行为，且代码的可读性非常差。

**【反例】**

```javascript
// 在控制性判断中赋值不易理解  
if (isFoo = false) {
  ...
}
```

**【正例】**

```javascript
const isFoo = someBoolean; // 在上面赋值，if条件判断中直接使用
if (isFoo) {
  ...
}
```
