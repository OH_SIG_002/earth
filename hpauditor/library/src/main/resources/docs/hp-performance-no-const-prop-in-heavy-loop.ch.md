## 热点循环中常量提取，减少属性访问次数

在实际的应用场景中抽离出来如下用例，其在循环中会大量进行一些常量的访问操作，该常量在循环中不会改变，可以提取到循环外部，减少属性访问的次数。

【反例】

``` TypeScript
// 优化前代码
private getDay(year: number): number {
  /* Year has (12 * 29 =) 348 days at least */
  let totalDays: number = 348;
  for (let index: number = 0x8000; index > 0x8; index >>= 1) {
    // 此处会多次对Time的INFO及START进行查找，并且每次查找出来的值是相同的
    totalDays += ((Time.INFO[year- Time.START] & index) !== 0) ? 1 : 0;
  }
  return totalDays + this.getDays(year);
}
```

可以将`Time.INFO[year - Time.START]`进行热点函数常量提取操作，这样可以大幅减少属性的访问次数，性能收益明显。

【正例】

``` TypeScript
// 优化后代码
private getDay(year: number): number {
  /* Year has (12 * 29 =) 348 days at least */
  let totalDays: number = 348;
  const info = Time.INFO[year - Time.START]; // 1. 从循环中提取不变量
  for (let index: number = 0x8000; index > 0x8; index >>= 1) {
    if ((info & index) !== 0) {
      totalDays++;
    }
  }
  return totalDays + this.getDays(year);
}
```