## 禁用不必要的return await

因为async function的返回值总是封装在Promise.resolve，return await实际上并没有做任何事情，只是在Promise resolve或reject之前增加了额外的时间。唯一有效的情况是，在try/catch语句中使用return await来捕获另一个基于Promise的函数的错误。

**【反例】**

```javascript
async function foo() {
  return await bar();
}

async function foo() {
  const baz = await bar();
  return baz;
}

```

**【正例】**

```javascript
async function foo() {
  return bar();
}
async function foo() {
  await bar();
  return;
}
async function foo() {
  const baz = await bar();
  //some real code here, for example
  let n = 42;
  return baz;
}
async function foo() {
  try {
    return await bar();
  } catch (error) {
    // here can be executed, go on
  }
}
```
