## 使用点号来访问对象的属性，只有计算属性使用[]

**【级别】规则**

**【描述】**

在JavaScript中，可以使用点号 (foo.bar) 或者方括号 (foo['bar'])来访问属性。然而，点号通常是首选，因为它更加易读，简洁，也更适于JavaScript压缩。

**【正例】**

```javascript
const name = obj.name;
const key = getKeyFromDB();
const prop = obj[key]; // 属性名是变量时才使用[]
```
