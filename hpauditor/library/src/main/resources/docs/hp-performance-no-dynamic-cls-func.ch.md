## 避免动态声明function与class

不建议动态声明function和class。

以如下用例为例，动态声明了class Add和class Sub，每次调用`foo`都会重新创建class Add和class Sub，对内存和性能都会有影响。

【反例】

``` TypeScript
function foo(f: boolean) {
  if (f) {
    return class Add{};
  } else {
    return class Sub{};
  }
}
```

【正例】

``` TypeScript
class Add{};
class Sub{};
function foo(f: boolean) {
  if (f) {
    return Add;
  } else {
    return Sub;
  }
}
```