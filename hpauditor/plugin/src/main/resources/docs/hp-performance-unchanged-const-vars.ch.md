## 不变的变量声明为const

不变的变量推荐使用const进行初始化。

【反例】

``` TypeScript
// 该变量在后续过程中并未发生更改，建议声明为常量
let N = 10000;

function getN() {
  return N;
}
```

【正例】

``` TypeScript
const N = 10000; 

function getN() {
  return N;
}
```