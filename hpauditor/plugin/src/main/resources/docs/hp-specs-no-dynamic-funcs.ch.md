## 不要动态创建函数


定义函数的方法包括3种：函数声明、Function构造函数和函数表达式。不管用哪种方法定义函数，它们都是Function对象的实例，并将继承Function对象所有默认或自定义的方法和属性。以函数构造器创建函数的方式类似于函数eval()，可以接受任何字符串形式作为它的函数体，这就会有安全漏洞的风险。

**【反例】**

```javascript
let add = new Function('a','b','return a + b');
// Function构造函数也可以只有一个参数，该参数可以为任意的字符串:
let dd = new Function('alert("hello")');
```

**【正例】**

```javascript
// 函数声明
function add(a,b){
  return a+b;
}
// 函数表达式
let add = function(a,b){
  return a+b;
}
```