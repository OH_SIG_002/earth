## 静态常量名、枚举值名采用全部大写，单词间使用下划线隔开。

**正例：**

```javascript
const MAX_USER_SIZE = 10000;

const UserType = {
  TEACHER: 0,
  STUDENT: 1
};
```
