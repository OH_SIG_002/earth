## 使用合适的数据结构

在实际的应用场景中抽离出来如下用例，该接口中使用JS Object来作为容器去处理Map的逻辑，建议使用HashMap来进行处理。

【反例】

``` TypeScript
getInfo(t1, t2) {
  if (!this.check(t1, t2)) {
    return "";
  }
  // 此处使用JS Object作为容器
  let info= {};  
  this.setInfo(info);
  let t1= info[t2];
  return (t1!= null) ? t1: "";
}
setInfo(info) {
  // 接口内部实际上进行的是map的操作
  info[T1] = '七六';   
  info[T2] = '九一';
  ... ...
  info[T3] = '十二';
}
```

代码可以进行如下修改，除了使用引擎中提供的标准内置map之外，还可以使用ArkTS提供的[高性能容器类](../arkts-utils/container-overview.md)。

【正例】

``` TypeScript
import HashMap from '@ohos.util.HashMap'; 

getInfo(t1, t2) {
  if (!this.check(t1, t2)) {
    return "";
  }
  // 此处替换为HashMap作为容器
  let info= new HashMap();
  this.setInfo(info);
  let t1= info.get(t2);
  return (t1!= null) ? t1: "";
}
setInfo(info) {
  // 接口内部实际上进行的是map的操作
  info.set(T1, '七六');   
  info.set(T2, '九一');
  ... ...
  info.set(T3, '十二');
}
```