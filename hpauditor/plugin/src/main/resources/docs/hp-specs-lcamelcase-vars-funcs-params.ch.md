## 变量名、方法名、参数名采用`lowerCamelCase`风格。

**正例：**

```javascript
let msg = 'Hello world';

function sendMsg(msg) {
  // todo send message
}

function findUser(userID) {
  // todo find user by user ID
}
```