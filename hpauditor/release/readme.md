## Library
请先在plugin.xml里注册toolwindow (factoryClass = `"com.example.devecohp.MyToolWindowFactory"`) 和codeInsight.lineMarkerProvider (language="", implementationClass=`"com.example.devecohp.AuditorLineMarker"`)

用例：
```xml
<extensions defaultExtensionNs="com.intellij">
    <toolWindow id="Test" icon="com.intellij.execution.testframework.sm.runner.ui.SMPoolOfTestIcons.FAILED_E_ICON" anchor="bottom" factoryClass="com.example.devecohp.MyToolWindowFactory"/>
    <codeInsight.lineMarkerProvider
            language=""
            implementationClass="com.example.devecohp.AuditorLineMarker"    />
  </extensions>
```
并确定jar在dependencies里 (确认build.gradle.kts里包含以下字段)
```
dependencies {
  implementation(files(jar 的路径))
}
```
比如说：
```
dependencies {
  implementation(files("..\\library\\build\\libs\\instrumented-HPAuditor-0.1-SNAPSHOT.jar"))
}
```

之后可以用AuditorTool里的函数来跑hpauditor并展示结果，参数是需要扫描的file的psi file 或者 file path + project 和hpauditor所在文件夹的路径

用例：扫描单个文件并展示结果(hpauditor 在"C:\\Users\\Blue Zone User\\Desktop\\New folder (2)\\" 里)
```java
(new AuditorTool()).auditFile(anActionEvent.getData(CommonDataKeys.PSI_FILE),"C:\\Users\\Blue Zone User\\Desktop\\New folder (2)\\");
(new AuditorTool()).auditFile(anActionEvent.getData(CommonDataKeys.PSI_FILE).getVirtualFile().getPath(),anActionEvent.getProject(),"C:\\Users\\Blue Zone User\\Desktop\\New folder (2)\\");
```
用例：扫描List里的文件并展示结果(hpauditor 在"C:\\Users\\Blue Zone User\\Desktop\\New folder (2)\\" 里)
```java
List<PsiFile> files = new ArrayList<>();
files.add(anActionEvent.getData(CommonDataKeys.PSI_FILE));
(new AuditorTool()).auditFiles(files,"C:\\Users\\Blue Zone User\\Desktop\\New folder (2)\\");
```
用例：扫描文件夹里的所有文件并展示结果(hpauditor 在"C:\\Users\\Blue Zone User\\Desktop\\New folder (2)\\" 里)
```java
VirtualFile virtualFile=anActionEvent.getData(CommonDataKeys.VIRTUAL_FILE);
PsiDirectory directory = PsiManager.getInstance(anActionEvent.getProject()).findDirectory(virtualFile);
(new AuditorTool()).auditDictionary(directory,"C:\\Users\\Blue Zone User\\Desktop\\New folder (2)\\");
```

## Plugin
请在files->setting->齿轮图标->Install Plugin from Disk 选择plugin-版本日期.zip 安装