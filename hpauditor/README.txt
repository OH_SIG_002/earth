Prototype Ark TS Code Auditor
J.R. Cordy et al., Huawei Technologies

August 2023 (Revised August 2023)

This is a rapid prototype of an Ark Typescript code auditor tool to check
and advise on proper Ark TS code usage.

To run the TS code auditor on an input, use the command:
	txl app.ts tsaudit.txl > app.ts.audit.ts
where "app.ts" is the input Ark TS source file to be audited and "app.ts.audit.ts" 
is the audited output source file.

JRC 12.9.23
