// Example OpenTxl-C embedded TXL application
// J.R. Cordy, Huawei Technologies, May 2024
 
// Standard C libraries
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Embedded TXL API 
// Using this interface, TXL programs can be embedded directly in other programs
#include <txlrun.h>

// The compiled TXL program
// By choosing a different _TXL name for each one, multiple TXL programs can be embedded in the same program
extern TXL TXL_TXL;

int main (int argc, char **argv)
{
    // This program takes as input a file of numbers, and invokes the embedded TXL program on the file ten times
    if (argc < 2) {
        printf ("Usage:  demo.x fileOfNumbers ...\n");
        exit (10);
    }

    // Run the TXL program on the input file ten times
    for (int i = 1; i < argc; i++) {
        char command[256] = "txl ";
        strncat (command, argv[i], 256);
        printf ("command %d: %s\n", i, command);
        int rc = TXL_runcmd (command, TXL_TXL);
        printf ("result code: %d\n", rc);
    }

    exit (0);
}
