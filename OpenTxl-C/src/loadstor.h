// OpentTxl-C Version 11 compiled app store/load facility
// Copyright 2023, James R Cordy and others

// Used only in txl.c main process
#ifdef TXLMAIN
// Initialization in txl.c main process
extern void loadstore (void);

// Used in txl.c compiled application load/store
extern void loadstore_save (string tofile);
extern void loadstore_restore (string fromfile);
#endif
