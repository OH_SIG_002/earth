// OpentTxl-C Version 11 input tokens
// Copyright 2023, James R. Cordy and others

#ifdef TXLMAIN
// Initialization
extern void tokens (void);
#endif

// Tokens are represented by their ident table index for efficiency
typedef int tokenT;             // allows maxIdents as large as we like
// #define NOT_FOUND 0             // actually index of chr (0), but that is meaningless

// Trees are represented by their tree table index
typedef int treePT;

// Kinds of trees in TXL trees - defined here so that we can encode
// literal kinds in the token table
// typedef unsigned char treeKindT;

enum treeKindT { 
    // structuring trees
    treeKind_order, treeKind_choose, treeKind_repeat, treeKind_list, 

    // structure generator trees
    treeKind_leftchoose, treeKind_generaterepeat, treeKind_generatelist, treeKind_lookahead, 
    treeKind_push, treeKind_pop, 

    // the empty tree
    treeKind_empty, 

    // leaf trees 
    treeKind_literal, treeKind_stringlit, treeKind_charlit, treeKind_token, 
    treeKind_id, treeKind_upperlowerid, treeKind_upperid, treeKind_lowerupperid, treeKind_lowerid, 
    treeKind_number, treeKind_floatnumber, treeKind_decimalnumber, treeKind_integernumber, 
    treeKind_comment, treeKind_key, treeKind_space, treeKind_newline, 
    treeKind_srclinenumber, treeKind_srcfilename, 

    // user specified leaves
    treeKind_usertoken1, treeKind_usertoken2, treeKind_usertoken3, treeKind_usertoken4, treeKind_usertoken5, 
    treeKind_usertoken6, treeKind_usertoken7, treeKind_usertoken8, treeKind_usertoken9, treeKind_usertoken10, 
    treeKind_usertoken11, treeKind_usertoken12, treeKind_usertoken13, treeKind_usertoken14, treeKind_usertoken15, 
    treeKind_usertoken16, treeKind_usertoken17, treeKind_usertoken18, treeKind_usertoken19, treeKind_usertoken20, 
    treeKind_usertoken21, treeKind_usertoken22, treeKind_usertoken23, treeKind_usertoken24, treeKind_usertoken25, 
    treeKind_usertoken26, treeKind_usertoken27, treeKind_usertoken28, treeKind_usertoken29, treeKind_usertoken30, 

    // special trees 
    treeKind_firstTime, treeKind_subsequentUse, treeKind_expression, treeKind_lastExpression, treeKind_ruleCall, 

    // undefined tree
    treeKind_undefined
};

// Order of the above is very important - it is used to optimize the transformation!
#define firstTreeKind           treeKind_order
#define firstStructureKind      treeKind_order
#define lastStructureKind       treeKind_list

#define firstLeafKind           treeKind_empty
#define firstLiteralKind        treeKind_literal
#define lastLiteralKind         treeKind_usertoken30
#define lastLeafKind            treeKind_usertoken30

#define firstSpecialKind        treeKind_firstTime
#define lastSpecialKind         treeKind_ruleCall
#define lastTreeKind            treeKind_undefined

// assert ((lastStructureKind < firstLeafKind) && (firstLiteralKind == firstLeafKind + 1) &&
//      (firstLeafKind == treeKindT.empty) && (firstLiteralKind == treeKindT.literal) && (lastLeafKind < firstSpecialKind));
            
#define firstUserTokenKind      treeKind_usertoken1
#define lastUserTokenKind       treeKind_usertoken30

// The input token table - assists in full backup parsing of both input and patterns
struct tokenTableT {
    tokenT token;        // ident index
    tokenT rawtoken;     // raw ident index
    enum treeKindT kind; // token/tree kind
    int    linenum;      // encoded source file and line number
    treePT tree;         // shared tree optimization - all instances of the same token share one tree
};

typedef int tokenIndexT;

// The input token table - used everywhere - 1-origin [1 .. maxTokens]
extern array (struct tokenTableT, inputTokens);
extern tokenIndexT currentTokenIndex, lastTokenIndex, failTokenIndex;

// Token kind to type name map - initialized by shared.c
extern tokenT kindType[lastTreeKind + 1];
