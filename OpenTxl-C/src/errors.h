// OpentTxl-C Version 11 error handling
// J.R. Cordy, Jan 2023

// Copyright 2023, James R. Cordy and others

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
// and associated documentation files (the “Software”), to deal in the Software without restriction, 
// including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies 
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
// AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// Error severity
#define INFORMATION     0
#define WARNING         1
#define LIMIT_WARNING   2

// Eventually fatal
#define DEFERRED        10

// Immediately fatal
#define FATAL           20
#define LIMIT_FATAL     21
#define INTERNAL_FATAL  22

#ifdef TXLMAIN
//Initialization
extern void errors (void);
#endif

// Source file names - 1-origin [1 .. maxFiles]
extern string fileNames[maxFiles + 1];
extern int nFiles;

// Stack exhaustion detection
extern addressint stackBase;
// extern void setStackBase (void);

// Used everywhere
extern void error (const string context, const string message, const int severity, const int code);
extern void externalType (const string internalType, string resultType);

// Used only in txl.c
extern void syntaxError (const tokenIndexT errorTokenIndex);

// Used only in xform_predef.h built-in functions
extern void predefinedParseError (const tokenIndexT errorTokenIndex, const tokenT rulename, const tokenT callername, const tokenT typename_, const string source);

#ifndef NOCOMPILE
// Used only in comprul.c rule compilation
extern void patternError (const tokenIndexT errorTokenIndex, const string context, const treePT productionTP);
#endif

// Used only in parse.c parser
extern void parseInterruptError (const tokenIndexT errorTokenIndex, const bool patternParse, const string context);
extern void parseStackError (const tokenIndexT errorTokenIndex, const bool patternParse, const string context);

