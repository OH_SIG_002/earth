// OpentTxl-C Version 11 Exception codes
// Copyright 2023, James R. Cordy and others

// System exceptions
#define QUIT            1       // clean quit
#define INTERRUPT       2       // ^C interrupt

// Global exceptions
#define ASSERTFAIL      501     // assertion failure
#define STACKLIMIT      502     // out of stack space

// Parser exceptions
#define CUTPOINT        503     // parser fence
#define CYCLELIMIT      504     // parser cycle limit
#define PARSETOODEEP    507     // parser depth limit

// Treespace exceptions
#define OUTOFKIDS       505     // out of kid space
#define OUTOFTREES      506     // out of tree space
