// OpentTxl-C Version 11 charset
// J.R. Cordy, Jan 2023

// Copyright 2023, James R. Cordy and others

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
// and associated documentation files (the “Software”), to deal in the Software without restriction, 
// including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies 
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
// AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// The TXL character set tables.
// Define and initialize character class tables used by the input scanner and output printer.
// Rapid character classification using array subscripting is key to fast scanning of input.
// Output spacing tables determine the default spacing between output tokens based on character adjacency.

// We support Unicode
#define LATIN1
#define UNICODE

// ASCII character set
#define ASCII 256
#define EOS '\0'

// Initialization of character property tables
#ifdef TXLMAIN
extern void charset (void);
#endif

// Character properties and maps
typedef bool charset_propertyT[ASCII];

// Character classes for Extended ASCII (Latin-1)
extern charset_propertyT charset_digitP, charset_alphaP, charset_alphaidP, charset_idP, charset_upperP, 
    charset_upperidP, charset_lowerP, charset_loweridP, charset_specialP, charset_repeaterP, 
    charset_optionalP, charset_separatorP, charset_spaceP, charset_metaP, charset_magicP;

// TXL string and character literal quote escape characters
extern char charset_stringlitEscapeChar;
extern char charset_charlitEscapeChar;

// TXL output spacing tables
extern charset_propertyT charset_spaceBeforeP;
extern charset_propertyT charset_spaceAfterP;

// Upper-to-lower and lower-to-upper case maps
typedef char charset_mapT[ASCII];
extern charset_mapT charset_uppercase;
extern charset_mapT charset_lowercase;

// Charset property test for entire strings
extern bool charset_uniformlyP (const longstring tokenText, const charset_propertyT propertyP);

// Modify charset properties
extern void charset_addIdChar (const char c, const bool setting);
extern void charset_addSpaceChar (const char c, const bool setting);
extern void charset_setEscapeChar (const char c, const bool setting);

// XML output encoding of special characters
extern void charset_putXmlCode (const int outstream, const longstring ls);
