// OpentTxl-C Version 11 input tokens
// J.R. Cordy, Jan 2023

// Copyright 2023, James R. Cordy and others

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
// and associated documentation files (the “Software”), to deal in the Software without restriction, 
// including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies 
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
// AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// The OpenTxl input token list.
// The scanner converts input text into this array of input tokens.

// Modification Log

// v11.0 Initial revision, adapted from OpenTxl 11.0

// I/O, strings, memory allocation
#include "support.h"

// Global limits
#include "limits.h"

// Check interface consistency
#include "tokens.h"

// 1-origin [1 .. maxTokens]
struct array (tokenTableT, inputTokens);

// The current, last and furthest accepted token in the table
tokenIndexT currentTokenIndex, lastTokenIndex, failTokenIndex;

// Initialization
void tokens (void) 
{
    // 1-origin [1 .. maxTokens]
    arrayalloc (maxTokens + 1, struct tokenTableT, inputTokens);
    inputTokens[0].token = UNUSED;
    lastTokenIndex = 0;
}
