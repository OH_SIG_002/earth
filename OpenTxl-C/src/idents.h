// OpentTxl-C Version 11 identifier/token hash table
// Copyright 2023, James R Cordy and others

// We use the hash code for ASCII NUL (chr(0)) as the nil identifier
#define nilIdent 0

#ifdef TXLMAIN
// Initialization
extern void ident (void);
#endif

// Used everywhere
extern array (longstring *, ident_idents);
extern array (enum treeKindT, ident_identKind);
extern tokenT ident_install (const string ident, const enum treeKindT kind);
extern int ident_nIdents;

// Used only in xform_garbage.h garbage collection
extern treePT   *ident_identTree;

// Used only in loadstor.c compiled application load/store
extern int ident_nIdents;
extern array (unsigned char, ident_identText);
extern int ident_nIdentChars;

// Used in comprul.c rule compilation, xform_debug.h rule debugger, xform_predef.h built-in functions
extern tokenT ident_lookup (const string ident);

// Used only in scan.c
extern void ident_setKind (const tokenT identIndex, const enum treeKindT kind);
